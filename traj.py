class Traj:
    """
    Should be bound to each Mod object and contain the basic info
    needed to perform a rerun; also stores the results of the rerun
    for later use
    """
    def __init__(self, ordering, path, temperature=300.0, top_num=0):
        self.path = path
        self.top_num = top_num
        self.topology = None  # a Top instance selected from instances stored in self.mod.top
        self.temperature = temperature
        self.derivatives = None
        self.rc = None
        self.observables = None
        self.ordering = ordering  # this is just an alias
        self.weights = None  # will be assigned a np.array containing original weights
        self.mod = None  # will be linked to the respective Mod instance
        
        
class Trajset:
    """
    Only useful for Quantity calculations, should bind several Traj
    instances in a single object irrespective of the Mod instance
    """
    def __init__(self, alias):
        self.traj_aliases = []
        self.alias = alias
        
    def append(self, new_traj):
        self.traj_aliases.append(new_traj.ordering)
    
    def yield_trajs(self, mod):
        return [t for t in mod.trajs if t.ordering in self.traj_aliases]
    
    def __bool__(self):
        if self.traj_aliases:
            return True
        return False
