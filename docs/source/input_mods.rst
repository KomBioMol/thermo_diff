.. _input_mods:

Mods input file
===============

The mods input specifies which atomistic properties should be modified during a ThermoDiff run.
Each modification requires a separate set of reruns, and can encompass any set of atoms (see below),
from a single atom in a single residue to a specific group of atoms in all given residues to
all atoms bearing a specified type.

ThermoDiff then calculates how the particular quantity (free energies, ensemble averaves, profiles etc.) respond
to a change in the given atomistic property.

Note that many lines can be contained in a single input file, each input letter ([cseadnm]) corresponding to a new
set of reruns:

+ ``c`` corresponds to a change in charge
+ ``s`` corresponds to a change in :math:`\sigma`
+ ``e`` corresponds to a change in :math:`\varepsilon`
+ ``a`` corresponds to a change in angle force constant
+ ``d`` corresponds to a change in dihedral force constant
+ ``n`` corresponds to a change in pairwise :math:`\sigma` (NBFIX)
+ ``m`` corresponds to a change in pairwise :math:`\varepsilon` (NBFIX)

Sample input lines
------------------

Modify charge/:math:`\sigma`/:math:`\varepsilon`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+ chosen atoms of a single type, all within a single residue (note: need to be of the same type):

.. code::

    cse AMFB-1 O11 O12 O13 O6 O7 O9 O10 O1 O2 O16

+ chosen atoms of a single type, possibly different residues (note: need to be of the same type):

.. code::

    cse AMFB-1-O11 AMFB-1-O12 AMFB-1-O13 AMFB-1-O6 AMFB-1-O7 AMFB-1-O9

+ all atoms of a specified type:

.. code::

    cse OG311

Modify angle/dihedral parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+ chosen sets of atoms, all within a single residue (possible multiples of 3 (angles) or 4 (dihedrals) in a single line):

.. code::

    d AMFB-1 C1 C2 C3 C4
    a AMFB-1 C1 C2 C3 C1 C2 C4

+ chosen sets of atoms, possibly different residues (note: need to have the same type signature):

.. code::

    d AMFB-1-C1 AMFB-1-C2 AMFB-1-C3 AMFB-1-C4
    a AMFB-1-C1 AMFB-1-C2 AMFB-1-C3 AMFB-1-C1 AMFB-1-C2 AMFB-1-C4

+ chosen set of types:

.. code::

    a CG311 CG311 OG311
    d OG311 CG311 CG311 OG311

Modify nbfix :math:`\sigma`/:math:`\varepsilon`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+ chosen sets of atoms, all within a single residue (";"-delimited; here O11, O12 and O13 need to have consistent types, as well as H1, H2 and H3)

.. code::

    nm AMFB-1 O11 O12 O13; H1 H2 H3

+ chosen sets of atoms, possibly different residues (";"-delimited; need to have consistent types, as above)

.. code::

    nm AMFB-1-O11 AMFB-1-O12 AMFB-1-O13; AMFB-1-H1 AMFB-1-H2 AMFB-1-H3

+ chosen pair of types:

.. code::

    nm HGA2 OG311

Note that 'n' refers to NBFIX-:math:`\sigma` and 'm' refers to NBFIX-:math:`\varepsilon`;
other cases should be intuitive ('c' for charge, 's' for sigma etc.)