.. _input_targets:

Targets input file
==================


In its force-field related applications, ThermoDiff attempts to find modifications that will alter the dynamics of
the system so that target quantities (e.g. experimentally measured radii of gyration, binding affinities,
force-extension curves, all-atom based free energy profiles) are achieved.

Again, many lines can be contained in a single input file, starting with
three letters \[fo\]\[ew\]\[pd\] followed by a user-specified keyword-value
pairs (see below for examples).

Run/target settings
-------------------

- concerning the type of target (mutually exclusive):

    + ``f`` sets free energy as a target

    + ``o`` sets any other observable (e.g. dihedral, distance, radius of gyration etc.) as a target

- concerning how the target quantity is set (mutually exclusive):

    + ``p`` indicates that the reference quantity is a profile (``ref=...`` should contain path to a similarly formatted file with the reference profile)

    + ``d`` indicates that the reference quantity is a discrete set of state-averaged quantities (``ref=...`` should contain a comma-separated list of values)

Keyworded arguments
-------------------

+ ``label=...`` sets an alias that can be used to reference the quantity later on

+ ``ref_profile=...`` sets a reference profile (path to a 2-column file with the reference profile)

+ ``ref_discrete=...`` sets a target for discrete states analysis (comma-separated floating-point values, one value for each state)

+ ``data=...`` refers to a file containing paths to files with user-defined observables (other than the reaction coordinate), one file per trajectory file (in the respective order)

+ ``meta=...`` points to a WHAM metafile (see below for formatting options) with reaction coordinate data

+ ``threshold=...`` should contain floating-point values defining boundaries between states; 2*n_states are needed (beginning and end of each state) so that non-adjacent states can be defined

+ ``trajset=...`` should pass a comma-separated list of trajectory groups (see :ref:`input_trajs` for reference)

+ ``relative=...`` should specify True if only differences between values are relevant (i.e. the quantity is defined up to an additive constant); True is default for free energy calculations, and False is default for other observables

Sample input lines
------------------

Free energy as a reference
~~~~~~~~~~~~~~~~~~~~~~~~~~

+ a free energy difference (-5 kcal/mol) between a bound and unbound state, calculated from US with bound state defined at up to 2 nm:

.. code::

    fd label=binding-affinity meta=/path/to/file/metafile.dat ref=-5.0,0.0 threshold=1.0,2.0,2.0,2.5

+ a free energy profile for the extension of a molecule, calculated from an equilibrium distribution

.. code::

    fp label=ext-curve meta=/path/to/file/metafile.dat ref_profile=/path/to/file/target_profile.dat

user-defined quantity as a reference
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+ a difference in dipole moment (2.5 D) between a bound and unbound state, calculated from an equilibrium distribution (bound state defined at up to 1.0 nm and unbound from 1.5 to 2.5 nm):

.. code::

    od label=dipole-moment meta=/path/to/file/metafile_distance.dat data=/path/to/file/dipole_moment_datafiles.dat ref=2.5,0.0 threshold=0.5,1.0,1.5,2.5 relative=True


+ a profile of a pseudo-dihedral in DNA along an A-to-B conformational transition, calculated from two sets of US runs (constituting only a specific subset of all trajectories):

.. code::

    op label=dihedral-profile meta=/path/to/file/metafile_conformational.dat data= ref_profile=/path/to/file/target_profile.dat trajset=us1,us2

metafile format
---------------

The metafile format for WHAM is designed to be similar to the popular Alan Grossfield's implementation,
i.e. each line should correspond to a single Umbrella Sampling window, and:

1. 1st column should contain the path to the datafile holding the original reaction coordinate (two-column, first column (times) is actually omitted, second column contains the reaction coordinate)

2.

    a) if no external potential was used (equilibrium runs only), no additional columns should be present
    b) if harmonic potentials were used, 2nd col should contain the center of potential in the given window
    c) if any other free energy method was used, 2nd col should contain the paths to files with calculated weights

3. if harmonic potentials were used, 3rd col should contain the force constant (in kcal/mol,
corresponding to k in the formula :math:`V = \frac{1}{2}k(x-x_0)^2`), and 4th col should contain F values
(obtained from running WHAM on original data).
