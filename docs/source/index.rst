
ThermoDiff
==========

ThermoDiff is a versatile Python tool that allows for efficient combination
of data from multiple Molecular Dynamics trajectories in order to

  + improve existing force fields,
  + reparametrize coarse grained (CG) models for consistency with all-atom models,
  + identify regions in molecules (e.g. ligands) that contribute to binding affinity or selectivity.

Overview
~~~~~~~~

ThermoDiff calculates (a) free energy derivatives or (b) observable derivatives with respect to
individual force field parameters, such as atomic charges :math:`q`,  Lennard-Jones
:math:`\sigma` or :math:`\varepsilon` (also with custom NBFIX combination rules), as well as bonded
terms: reference bond lengths, planar and dihedral angles, or force constants.

The general idea is to let the user provide as much data as possible - ideally multiple trajectories for
a number of molecular setups - then select a number of force field parameters to investigate, and
a subset of quantities of interest that are e.g. experimentally known constraints. The analysis should
yield a 'sensitivity matrix', an estimate of sensitivity of individual quantities to individual
force field parameters.

As a result, the user can make informed decisions about which parameters should be changed in order to
efficiently improve the poorly performing properties of the model, while retaining the properties that
currently agree with existing data. In principle, the three areas mentioned above - force field development,
coarse-graining of atomistic models and lead optimization in drug design - could benefit the most
from this approach.

.. toctree::
   :maxdepth: 2

   theory
   input_mods
   input_targets
   input_trajs
   ff_param
   cg_param
   drug_design
