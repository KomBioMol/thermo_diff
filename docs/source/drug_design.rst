.. _drug_design:

Drug design
===========

ThermoDiff can be applied to lead optimization in a straightforward way, providing the location of "hot spots" -
sites at which substituents should have a strong impact on the desired thermodynamic properties.

Affinity modulation
-------------------

To search for sites that increase affinity, one needs to provide trajectories that sample two states:
bound and unbound. Selected "query" sites have to be selected manually as independent lines in the
:ref:`input_mods`. To define states A and B, datafiles can be provided for each trajectory in which
frames corresponding to the bound state have value 0, and ones sampled from the unbound state have value 1.
A list of these two-column data files, corresponding to the :ref:`input_trajs`, has to be stored in a separate file.

Now, the following line in the :ref:`input_targets` requests the desired calculation (assume equilibrium simulations of both states):

.. code::

    fd label=binding-affinity meta=/path/to/file/meta.dat ref=B threshold=-0.5,0.5,0.5,1.5

The above line indicates that:

+ we use free energy as target quantity (``f``) using the difference between two states as target (``d``)

+ the quantity will be labeled as ``binding-affinity``

+ ``/path/to/file/meta.dat`` will be used to locate datafiles with reaction coordinate data

+ we seek to increase the free energy of state B (unbound; or, equivalently, lower the free energy of state A, bound; we follow the natural convention that the lower the free energy, the more favorable the respective state)

+ state A is defined from -0.5 to 0.5, and state B from 0.5 to 1.5 (artificial bounds chosen so that 0 falls into A and 1 into B)

Selectivity modulation
----------------------

To increase selectivity, an identical procedure can be used as above, as instead of increasing the free energy of the
unbound state we wish to increase the free energy of the deprecated bound state with respect to the preferred bound
state.

Note that we can attempt to simultaneously increase *both* affinity and selectivity simply by defining two target quantities
and labeling them as follows:

+ 0 for the unbound state,

+ 1 for the preferred bound state,

+ 2 for the deprecated bound state

so that the input looks like this:

.. code::

    fd label=binding-affinity data=/path/to/file/meta.dat ref=A threshold=-0.5,0.5,0.5,1.5
    fd label=selective-affinity data=/path/to/file/meta.dat ref=B threshold=0.5,1.5,1.5,2.5

Now we first attempt to increase the free energy of state A (unbound) with respect to B (preferred bound),
and then to increase the free energy of state B (deprecated bound) with respect to A (preferred bound).