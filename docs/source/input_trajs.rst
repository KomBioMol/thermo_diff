.. _input_trajs:

Trajectories input file
=======================


It can be convenient to restrict the parametrization of certain quantities to a subset of data,
e.g. if enhanced sampling techniques such as Umbrella Sampling were performed on different coordinates,
resulting in several sets of trajectories sampling different regions of configurational space.
For this reason, trajectories can be clustered into user-defined groups and passed along with target quantities
so that only the selected subset(s) will be used for target optimization.

If the ``trajset=...`` argument is not passed in the targets input (see :ref:`input_targets`),
by default all trajectories are used to optimize the given target.

Input format
------------

Each group should be named first (with a colon at the beginning of a line), then a set of paths to trajectory files
belonging to that group should follow.

Trajectories can be followed by temperature (default: 300) and a 0-based index of topology, if
multiple topologies are passed to the main script (if different trajectories correspond to
different systems).

Sample input
------------

Multiple groups:

.. code::

    :group1
    /home/user/simulations/mysystem/traj1_us.xtc
    /home/user/simulations/mysystem/traj2_us.xtc
    /home/user/simulations/mysystem/traj3_us.xtc
    :group2
    /home/user/simulations/mysystem/traj1_eq.xtc
    /home/user/simulations/mysystem/traj2_eq.xtc
    /home/user/simulations/mysystem/traj3_eq.xtc

Multiple topologies:

.. code::

    /home/user/simulations/mysystem1/traj1_eq.xtc 300 0
    /home/user/simulations/mysystem1/traj2_eq.xtc 300 0
    /home/user/simulations/mysystem2/traj1_eq.xtc 300 1
    /home/user/simulations/mysystem2/traj2_eq.xtc 300 1

when running the script with:

.. code::

    -p "/path/to/mysystem1/topol.top /path/to/mysystem2/topol.top "
