
Force field refinement
======================

One common problem in force field refinement is the optimal choice of parameters to improve, given the
vast amount of them required to accurately represent the behavior of complex macromolecules. While
highly accurate quantum chemical data can be used as reference, the inclusion of ensemble-averaged solvation
and competition effects is usually beyond the capabilities of quantum chemistry.

Hence, such bottom-up approaches might benefit from being complemented by a top-down one. Indeed,
ThermoDiff attempts to manipulate force field parameters so as to reproduce known properties of the
systems under study, given the states in question are properly sampled in simulations, and per-frame
statistical weights are available.

The kind of questions ThermoDiff was designed to answer is: "Which parameters should I touch if I want
to simultaneously make process X more favorable, increase average Y and retain the fraction of Z
from the current force field?". After that, the parameters need to be changed and tested empirically
anyway, but there is a good chance one won't be lost navigating the vast sea of overfitting and tradeoffs.

