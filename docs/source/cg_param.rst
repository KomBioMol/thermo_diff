
Coarse-grained model optimization
=================================

With ThermoDiff, CG models can be improved to match either experimental data (in a similar way to AA force field
reparametrization) or AA-derived quantities, such as ensemble averages or profiles of any observables, as well as
free energies.

A typical procedure aimed at improving the parameters of a particular CG model would be to perform a set of
all-atom enhanced sampling simulations (e.g. umbrella sampling, or replica-exchange metadynamics coupled to
an equilibrium ensemble) and then ask ThermoDiff to reproduce these quantities on the CG level; it is
particularly reasonable to try to optimize the NBFIX (i.e. custom combination rules) components,
as these allow to fine-tune the interactions between specific beads without affecting global properties of the
molecular model. If one however aims to optimize global properties, the optimization of :math:`\sigma`,
:math:`\varepsilon` and atomic charges can also be accomplished with ThermoDiff.
