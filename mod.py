from itertools import combinations
import os
from shutil import copy2


class Mod:
    counter = 0  # only used for naming with unique numbers
    
    def __init__(self, top, master, resnr='', resnames='', names=''):
        """
        Base class to work with parameter modifications.
        The class is actually used for the calculation of original
        (unmodified) quantities, and is subclassed by ModParam,
        ModNbfix and ModAtom.
        :param top: list of Top objects corresponding to the modified topology
        :param resnr: a list of tuples, each containing a list of 3 or 4 residue numbers
        :param resnames: a list of tuples, each containing a list of 3 or 4 residue names
        :param names: a list of tuples, each containing a list of 3 or 4 atom names
        """
        Mod.counter += 1
        self.td = master
        self.counter = Mod.counter
        self.top = top
        self.alch = self.td.alch
        self.qs = []  # list of target quantities to calculate/compare
        self.data = None
        self.diff = None
        self.meta = None
        self.topname = ''
        self.trajs = []
        self.dpar = 0.001
        self.sigma, self.chg, self.eps, self.nbs, self.nbe, self.dih, self.ang, self.nothing = [''] * 8
        assert bool(names) == bool(resnames) == bool(resnr)
        [self.sort_dihedrals(t) for t in self.top]
        if all([names, resnames, resnr]):
            self.names = names
            self.res = resnr
            self.resnames = resnames
            assert len(self.names) == len(self.res) == len(self.resnames)
        elif not any([names, resnames, resnr]):
            self.nothing = 'x'
    
    def __str__(self):
        """
        string representation of a Mod object, should be unique
        :return: mod one-letter code + Mod index, 1-based
        """
        return "{}-{}".format(''.join([self.sigma, self.chg, self.eps, self.nbs, self.nbe, self.dih, self.ang,
                                       self.nothing]), self.counter)
    
    def __eq__(self, other):
        """
        assumes unique numbering of instances
        :param other: another Mod (or subclass) instance to compare with
        :return: bool
        """
        return str(self) == str(other)

    def clone_type(self, top, atomtype, prefix):
        """
        adds modified (Y-prefixed) FF parameters if sigma
        needs to be modified to ensure the modification only
        affects the specific subset of atoms
        :param atomtype: str, type to be cloned
        :param prefix: str, prefix to be added
        :return: None
        """
        param_sections = ['atomtypes', 'pairtypes', 'bondtypes', 'constrainttypes',
                          'angletypes', 'dihedraltypes', 'nonbond_params']
        to_check = [i for i in range(len(top.section_headers)) if
                    top.section_headers[i] in param_sections]
        for s in to_check:
            section = top.sections[s][:]
            to_add = []
            for l in range(len(section)):
                line = section[l]
                if len(line.split()) > 4 and not line[0] in ';[' \
                        and any([line.split()[i] == atomtype for i in range(4)]) \
                        and not any([str(prefix+atomtype) in line.split()[i] for i in range(4)]):
                    to_add.append(line)
            for line in to_add:
                newlines = self.gen_clones(line, atomtype, prefix)
                section.extend(newlines)
            top.sections[s] = section

    def gen_clones(self, line, atomtype, prefix, permutations=False):
        """
        copies the original parameters onto the modified type,
        also taking care of multiple modifications per line;
        the goal is to have both original terms,
        fully modified terms as well as all possible combinations
        of these; e.g. if we wish to clone "CA" in a line
        CA CA CT 1
        the result should be
        CA CA CT 1
        YCA CA CT 1
        CA YCA CT 1
        YCA YCA CT 1
        :param line: str, a line with parameter definition to be copied
        :param atomtype: str, type to be cloned
        :param prefix: str, prefix to be added
        :param permutations: bool, whether to generate all permutations
        :return: list, contains modified lines
        """
        lines = []
        nchanges = line.split().count(atomtype)
        if permutations:
            mods = []
            for i in range(nchanges):
                mods.extend(self.gen_combs(nchanges, i + 1))
        else:
            mods = [tuple(range(nchanges))]
        for m in mods:
            lines.append(self.mod_types(line, m, prefix, atomtype))
        return lines

    @staticmethod
    def mod_types(line, mods, prefix, atomtype):
        """
        takes a line and a tuple of numbers that specifies at which
        occurrences of the type the modification has to take place,
        and performs the requested modifications
        :param line: str, line to be modified
        :param mods: tuple, contains ints according to description
        :param prefix: str, prefix to be added
        :param atomtype: str, type to be modified
        :return: str, modified line
        """
        for num in mods[::-1]:
            indices = [i for i in range(len(line) - len(atomtype) + 1)
                       if line[i:i + len(atomtype)] == atomtype
                       and (i == 0 or line[i-1].isspace())
                       and (i+len(atomtype) == len(line) or line[i+len(atomtype)].isspace())]
            line = line[:indices[num]] + prefix + line[indices[num]:]
        return line
    
    @staticmethod
    def get_type(top, name, resnr, resname):
        """
        finds the type of an atom specified by its name
        and residue number (as given in the .top file)
        :param name: str, atom name
        :param resnr: int, residue number
        :param resname: str, residue name
        :return: str, atom type
        """
        for section in top.list_sections('atoms'):
            for line in top.sections[section]:
                lspl = line.split()
                if len(lspl) > 6 and lspl[4] == name and int(lspl[2]) == int(resnr) and lspl[3] == resname:
                    return lspl[1]
        raise RuntimeError("atom {} not found in residue {}".format(name, resnr))
    
    @staticmethod
    def gen_combs(count, tuples):
        """
        generates a list of possible modifications to be done on lines
        containing more than one occurrence of the type to be
        modified
        :param count: int, how many times the type to be changed occurs
        in the line
        :param tuples: int, how many modifications need to be performed
        :return: list, includes tuples containing occurrences to modify
        e.g. gen_combs(3, 2) denote three occurrences and two modifications
        -> [(0, 1), (0, 2), (1, 2)] are the possible combinations
        """
        return list(combinations(range(count), tuples))

    def sort_dihedrals(self, top):
        """
        sorts dihedrals in 'dihedraltypes' section to ensure that
        multi-component dihedrals are clustered together, as
        required by gromacs
        :return: None
        """
        if 'dihedraltypes' in top.section_headers:
            section = top.list_sections('dihedraltypes')[0]
            top.sections[section].sort(key=self.sorting_fn)

    @staticmethod
    def sorting_fn(line):
        """
        ensures that (1) multiple component dihedrals are placed in consecutive
        lines and (2) wildcards are placed at the end (otherwise are ignored)
        :param line: str, line to be assessed for sorting
        :return: int, sorting priority
        """
        lspl = line.split()
        if len(lspl) > 4 and lspl[0][0] not in ';[':
            value = sum(map(ord, lspl[0])) + 10**3*sum(map(ord, lspl[1])) \
                   + 10**6*sum(map(ord, lspl[2])) + 10**9*sum(map(ord, lspl[3]))
            if "X" not in lspl[:4]:
                return value
            else:
                return value + 10**12
        elif len(lspl) > 2 and lspl[0][0] == '[':
            return -1
        else:
            return 0
    
    def save_mod(self, path, name):
        """
        wraps Top's save_mod to catch the file name
        :param path: str, filename to save .top to
        :param name: str, name of the file
        :return: None
        """
        for t in self.top:
            self.topname = path
            final_name = self.topname + name + '-' + t.top
            if not os.path.exists(final_name):
                t.save_mod(final_name)
            else:
                print(f'file {final_name} already exists, leaving as it is')
    
    def goto_mydir(self):
        """
        shortcut to the ModAtom's working directory
        :return: None
        """
        os.chdir(self.topname)
    
    def add_weights_to_meta(self, meta, trajs):
        """
        adds weights to datafiles from US so that after reruns are completed
        WHAM can be re-performed with updated weights, and writes the updated metafile
        :param meta: str, path to the original WHAM metafile
        :param trajs: list, contains trajectory filenames
        :return: str, name of the new meta file
        """
        self.goto_mydir()
        wfiles = [tr.weights for tr in trajs]
        meta_dir = '/'.join(meta.split('/')[:-1])
        meta_content = [line.strip() for line in open(meta)]
        if not meta_content[0].startswith("/"):
            meta_content = [meta_dir + '/' + path for path in meta_content]
        assert len(wfiles) == len(meta_content)
        # first get orig colvar data and paste weights as third column
        for n in range(len(meta_content)):
            fname = meta_content[n].split()[0]
            copy2(fname, os.getcwd())
            self.paste_two(fname.split('/')[-1], wfiles[n])
        # then create new metas with relative paths
        # in case original ones were paths, not filenames
        # (likely to be the same as original)
        new_meta_content = []
        for line in meta_content:
            newline = line.split()[0].split('/')[-1] + ' ' + ' '.join(line.split()[1:]) + '\n'
            new_meta_content.append(newline)
        meta_name = os.getcwd() + '/' + meta.split('/')[-1]
        with open(meta_name, 'w') as outfile:
            for line in new_meta_content:
                outfile.write(line)
        return meta_name
    
    def paste_two(self, file1, content2):
        """
        pastes two file, writing the result to the first of the two
        :param file1: str, first file to process
        :param content2: list of strings, imitates the second file to process
        :return: None
        """
        f1_cont = self.remove_duplicates([x.strip() for x in open(file1) if not x.startswith(('#', '@'))])
        f2_cont = self.remove_duplicates(["{} {}".format(*list(x)) for x in content2])
        f2_cont = [x.split()[1] for x in f2_cont]
        if len(f1_cont) != len(f2_cont):
            raise RuntimeError('The number of non-unique lines in file {f1} does not match the corresponding '
                               'energy.xvg file'.format(f1=file1))
        new_cont = [a + ' ' + b + '\n' for a, b in zip(f1_cont, f2_cont)]
        with open(file1, 'w') as outfile:
            for line in new_cont:
                outfile.write(line)

    def remove_duplicates(self, dup_list):
        """
        removes duplicates
        :param dup_list: list, contains potential duplicates
        :return: list, now without duplicates
        """
        dup_list = list(set(dup_list))
        dup_list.sort(key=self.sort_dups)
        # i = 0
        # while i < len(dup_list) - 1:
        #     if dup_list[i] in dup_list[i + 1:]:
        #         dup_list.pop(i)
        #         i -= 1
        #     if i%100 == 0:
        #         print(i)
        #     i += 1
        return dup_list
    
    @staticmethod
    def sort_dups(line):
        return float(line.split()[0].strip())
