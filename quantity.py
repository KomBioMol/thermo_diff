import numpy as np
from itertools import product
from scipy.interpolate import interp1d
from scipy.integrate import trapz, quad
from scipy.stats import binned_statistic
from processing import read_xvg
from skimage.filters import gaussian


class Quantity:
    """
    A class to hold data concerning one of the target (reference) quantities
    used in force field refinement, and compare with the reweighted sims
    :param target: a Target instance
    :param mod: str, contains 'f' or 'o' for free energy or other observable,
                              'w' or 'e' for wham or equilibrium simulation data,
                              'p' or 'd' if target quantity is a profile or set of discrete values
    """
    def __init__(self, target, mod):
        self.target = target
        self.mod = mod
        self.metafile = self.target.meta
        self.datafile = self.target.orig_data
        self.name = self.target.label
        self.trajset = self.target.trajset
        self.trajs = self.trajset.yield_trajs(self.mod)
        self.threshold = self.target.threshold  # now is a path to file
        if self.threshold is not None:
            threshold = [x for line in open(self.threshold) for x in line.strip().split(',')]
            self.threshold = [threshold[2*i:2*(i+1)] for i in range(len(threshold)//2)]
        self.target_data = self.target.target_data  # now is a path to file
        ####
        self.f = True if 'f' in self.target.mode else False  # free energy as target
        self.o = True if 'o' in self.target.mode else False  # other quantity (arbitrary observable)
        ####
        self.p = True if 'p' in self.target.mode else False  # target given as a reference profile
        self.d = True if 'd' in self.target.mode else False  # target given as a discrete set of state-avgd quantities
        ####
        self.discrete = False
        self.discrete_states = []
        self.weights = None
        self.diff = None
        self.grid = None
        self.bin_edges = None
        self.profile = None
        self.sensitivity = 0  # defined as d/dsigma ((mod-orig)/(target-orig))
        self.multiplier = mod.dpar
        self.probability_profile = None
        self.observable_profile = None
        self.free_energy_profile = None
        self.free_energy_derivative = None
        self.discrete_free_energy_derivative = None
        self.discrete_observable_derivative = None
        self.observable_derivative = None
        self.product_profile = None

    def calc_perframe_weights(self):
        """
        reads in traj.weights and traj.rc for all trajs
        also sets self.grid
        :return:
        """
        meta_data = [line.strip().split() for line in open(self.metafile) if line.strip()]
        if not all([len(line) == len(meta_data[0]) for line in meta_data]):
            raise RuntimeError("Not all lines in metafile have the same number of columns, "
                               "please fix this before proceeding")
        rc_files = [line[0] for line in meta_data]
        if len(meta_data[0]) == 1:
            for traj, rc in zip(self.trajs, rc_files):
                try:
                    traj.weights = np.ones(shape=len(np.loadtxt(rc)))
                except ValueError:
                    traj.weights = np.ones(shape=len([line for line in open(rc) if line.strip()]))
        elif len(meta_data[0]) == 2:
            weight_files = [line[1] for line in meta_data]
            for traj, wf in zip(self.trajs, weight_files):
                traj.weights = np.loadtxt(wf)
        elif len(meta_data[0]) == 4:
            try:
                rcs = [np.loadtxt(x)[:, 1] for x in rc_files]
            except ValueError:
                raise ValueError("WHAM-based weighting not allowed with char-based state labels")
            except IndexError:
                print("Only 1 column found in data files, assuming this is the reaction coordinate data")
                rcs = [np.loadtxt(x) for x in rc_files]
            centers = [float(line[1]) for line in meta_data]
            ks = [float(line[2]) for line in meta_data]
            fs = [float(line[3]) for line in meta_data]
            for traj, rc, c, k, f in zip(self.trajs, rcs, centers, ks, fs):
                traj.weights = np.exp(((0.5*k*(rc-c)**2)-f)/(self.mod.td.kt/4.184))
        sum_weights = np.sum(np.concatenate([t.weights for t in self.trajs]))
        for traj, rc in zip(self.trajs, rc_files):
            traj.weights = traj.weights/sum_weights
            try:
                traj.rc = np.loadtxt(rc)[:, 1]
            except ValueError:
                try:
                    traj.rc = [line.strip().split()[1].upper() for line in open(rc)]
                except IndexError:
                    print("Only 1 column found in {}, assuming this is the reaction coordinate data".format(rc))
                    traj.rc = [line.strip().split()[0].upper() for line in open(rc)]
                self.discrete = True
                traj.rc = np.array(traj.rc)
            except IndexError:
                print("Only 1 column found in {}, assuming this is the reaction coordinate data".format(rc))
                traj.rc = np.loadtxt(rc)
            else:
                pass
        if self.discrete:
            self.discrete_states = sorted(list(set([rc for traj in self.trajs for rc in traj.rc])), key=ord)
        else:
            all_rc = np.concatenate([traj.rc for traj in self.trajs])
            mn, mx = np.min(all_rc), np.max(all_rc)
            self.grid = np.linspace(mn, mx, 100)
            grid_spacing = self.grid[1] - self.grid[0]
            self.bin_edges = np.append(self.grid - 0.5 * grid_spacing, [self.grid[-1] + 0.5 * grid_spacing])

    def calc_perframe_derivatives(self):
        if self.mod.td.alch:
            for filename, traj in [('{}-dhdl.xvg'.format(tr.ordering), tr) for tr in self.trajs]:
                traj.derivatives = np.loadtxt(filename, comments=['#', '@'])[:, 1]/self.mod.dpar
        else:
            for filename, traj in [('{}-energy.xvg'.format(tr.ordering), tr) for tr in self.trajs]:
                ref_eners = np.loadtxt(f'../x-1/{filename}', comments=['#', '@'])[:, 1]
                own_eners = np.loadtxt(filename, comments=['#', '@'])[:, 1]
                traj.derivatives = (own_eners-ref_eners)/self.mod.dpar

    def get_perframe_observables(self):
        if self.datafile is not None:
            dfiles = [line.strip() for line in open(self.datafile)]
            if len(dfiles) != len(self.trajs):
                raise RuntimeError(f"Number of datafiles in {self.datafile} does not match the number of trajectories "
                                   "in trajset {self.trajset.alias}")
            for traj, df in zip(self.trajs, dfiles):
                traj.observables = np.loadtxt(df)

    def calc_all_profiles(self):
        if self.discrete:
            self.discrete_free_energy_derivative = self.calc_discrete_derivs()
        else:
            self.probability_profile, _, _ = binned_statistic(np.concatenate([traj.rc for traj in self.trajs]),
                                                              np.concatenate([traj.weights for traj in self.trajs]),
                                                              bins=self.bin_edges, statistic="sum")
            self.free_energy_profile = -self.mod.td.kt * np.log(self.probability_profile)
            self.free_energy_profile -= np.min(self.free_energy_profile)
            self.free_energy_derivative = self.calc_ens_avg_profile('derivatives')
            self.free_energy_derivative = gaussian(self.free_energy_derivative.reshape(-1, 1), 2).reshape(-1)
            if self.datafile is not None:
                self.observable_profile = self.calc_ens_avg_profile('observables')
                self.product_profile = self.calc_ens_avg_profile('observables', 'derivatives')
                self.observable_derivative = (self.observable_profile*self.free_energy_derivative -
                                              self.product_profile)/self.mod.td.kt
                self.observable_derivative = gaussian(self.observable_derivative.reshape(-1, 1), 2).reshape(-1)
            if self.threshold:
                if len(self.threshold) == 2:
                    self.discrete_free_energy_derivative = np.array([self.discrete_free_derivative()])
                else:
                    self.discrete_free_energy_derivative = np.array(self.multi_discrete_free_derivative())

    def calc_discrete_derivs(self):
        mean_der = [np.sum(np.concatenate([traj.derivatives * traj.weights * (traj.rc == state)
                                           for traj in self.trajs])) /
                    np.sum(np.concatenate([traj.weights * (traj.rc == state)
                                           for traj in self.trajs])) for state in self.discrete_states]
        if len(self.discrete_states) > 1:
            rel_derivatives = [mean_der[0] - der for der in mean_der[1:]]
        else:
            rel_derivatives = mean_der
        return np.array([[deriv, 0] for deriv in rel_derivatives]).ravel()  # TODO implement stderr

    def calc_ens_avg_profile(self, attr_name, second_attr=None):
        if second_attr is None:
            num, _, _ = binned_statistic(np.concatenate([traj.rc for traj in self.trajs]),
                                         np.concatenate([traj.weights * traj.__getattribute__(attr_name)
                                                         for traj in self.trajs]),
                                         bins=self.bin_edges, statistic="sum")
        else:
            num, _, _ = binned_statistic(np.concatenate([traj.rc for traj in self.trajs]),
                                         np.concatenate([traj.weights * traj.__getattribute__(attr_name) *
                                                         traj.__getattribute__(second_attr)
                                                         for traj in self.trajs]),
                                         bins=self.bin_edges, statistic="sum")
        den, _, _ = binned_statistic(np.concatenate([traj.rc for traj in self.trajs]),
                                     np.concatenate([traj.weights for traj in self.trajs]),
                                     bins=self.bin_edges, statistic="sum")
        return num/den

    def write_derivatives(self):
        if self.free_energy_derivative is not None:
            np.savetxt("free_energy_derivative_m{}_{}.dat".format(self.mod.counter, self.name),
                       np.vstack([self.grid, self.free_energy_derivative]).T, fmt='%10.3f')
        if self.discrete_free_energy_derivative is not None:
            np.savetxt("discrete_free_energy_derivative_m{}_{}.dat".format(self.mod.counter, self.name),
                       self.discrete_free_energy_derivative, fmt='%10.3f')
        if self.datafile is not None and self.observable_derivative is not None:
            np.savetxt("observable_derivative_m{}_{}.dat".format(self.mod.counter, self.name),
                       np.vstack([self.grid, self.observable_derivative]).T, fmt='%10.3f')
        if self.discrete_observable_derivative is not None:  # TODO implement
            pass

    def discrete_free_derivative(self):
        assert self.threshold is not None
        avg_a, std_a = self.free_energy_derivative_definite(*self.threshold[0])
        avg_b, std_b = self.free_energy_derivative_definite(*self.threshold[1])
        return np.array([avg_a - avg_b, np.sqrt(std_a**2 + std_b**2)])

    def multi_discrete_free_derivative(self):
        assert self.threshold is not None
        derivatives = []
        for i in range(len(self.threshold) - 1):
            avg_a = self.free_energy_derivative_definite(*self.threshold[0])
            avg_b = self.free_energy_derivative_definite(*self.threshold[i+1])
            derivatives.append(avg_a - avg_b)
        return derivatives

    def free_energy_derivative_definite(self, lower, upper):
        if ';' in lower or ';' in upper:
            lowers = [float(x) for x in lower.split(';')]
            uppers = [float(x) for x in upper.split(';')]
            results = [self._free_energy_derivative_definite(*lu) for lu in product(lowers, uppers)]
        elif ':' in lower or ':' in upper:
            lowers = [float(x) for x in lower.split(':')]
            uppers = [float(x) for x in upper.split(':')]
            assert len(lowers) == len(uppers)
            results = [self._free_energy_derivative_definite(lo, up) for lo, up in zip(lowers, uppers)]
        else:
            results = [self._free_energy_derivative_definite(float(lower), float(upper))]
        results = np.array(results)
        return np.array([np.mean(results), np.std(results)])

    def _free_energy_derivative_definite(self, lower, upper):
        prob = interp1d(self.grid, np.exp(-self.free_energy_profile/self.mod.td.kt))
        deriv_mult_prob = interp1d(self.grid,
                                   self.free_energy_derivative * np.exp(-self.free_energy_profile/self.mod.td.kt))
        num = quad(deriv_mult_prob, lower, upper)[0]
        den = quad(prob, lower, upper)[0]
        return num/den


    # def calc_derivatives(self):
    #     td = self.mod.td
    #     from skimage.filters import gaussian
    #     datafiles = [x.strip() for x in open(self.datafile)]
    #     data = [read_xvg(x, self.mod)[:, 1] for x in datafiles]
    #     derivatives = [tr.derivatives for tr in self.trajset.yield_trajs(self.mod)]
    #     if not self.mod.meta:
    #         for d, tr in zip(data, self.trajset.yield_trajs(self.mod)):
    #             tr.weights = np.ones(len(d))
    #     self.profile, self.grid = td.run_binner(target='free', q=self)
    #     spacing = self.grid[1] - self.grid[0]
    #     bin_edges = np.concatenate((self.grid - 0.5 * spacing, [self.grid[-1] + 0.5 * spacing]))
    #     weights_cat = np.concatenate([tr.weights for tr in self.trajset.yield_trajs(self.mod)])
    #     numerator, bin_edges, _ = binned_statistic(np.concatenate(data),
    #                                                np.concatenate(derivatives)*weights_cat,
    #                                                bins=bin_edges, statistic=sum)
    #     denominator, bin_edges, _ = binned_statistic(np.concatenate(data),
    #                                                  weights_cat,
    #                                                  bins=bin_edges, statistic=sum)
    #     self.derivative = (numerator/(denominator+10**(-10))).reshape(-1, 1)
    #     self.derivative = gaussian(self.derivative, 2).reshape(-1)
    #     extrapolated_profile = self.profile + self.derivative - np.nanmean(self.derivative)
    #     np.savetxt("derivative_m{}_{}.dat".format(self.mod.counter, self.name), self.derivative, fmt='%10.3f')
    #     np.savetxt("orig_profile_m{}_{}.dat".format(self.mod.counter, self.name), self.profile, fmt='%10.3f')
    #     np.savetxt("mod_profile_m{}_{}.dat".format(self.mod.counter, self.name), extrapolated_profile, fmt='%10.3f')

    def read_data(self):
        if self.d:
            self.data = self.get_discrete()

    # def combine_weights(self, td):
    #     # TODO all
    #     if self.weights:  # we've run WHAM before
    #         pass
    #     elif self.e and self.target.meta and not td.alch:
    #         weights_extern = [np.loadtxt(f.strip().split()[2]) for f in open(self.target.meta) if f.strip()]
    #         assert len(weights_extern) == len(self.trajset.yield_trajs(self.mod))
    #         self.weights = [wext * traj.weights[:, 1] for wext, traj in
    #                         zip(weights_extern, self.trajset.yield_trajs(self.mod))]
    #     elif self.e and self.target.meta and td.alch:
    #         self.weights = [np.loadtxt(f.strip().split()[2]) for f in open(self.target.meta) if f.strip()]
    #     else:
    #         self.weights = [t.weights for t in self.trajset.yield_trajs(self.mod)]
    #     return self.weights
    
    # def get_discrete(self):
    #     discrete_data = []
    #     if self.f:
    #         if self.w:
    #             ddata = []
    #             pmf_interp = interp1d(self.grid, self.profile)
    #             # TODO enable variable temperature for each quantity?
    #             kt = 8.314*300/4184
    #             for i in range(len(self.threshold)-1):
    #                 integr_grid = np.linspace(self.threshold[i], self.threshold[i+1], 500)
    #                 avg = -kt * np.log(trapz(np.array([np.exp(-pmf_interp(x)/kt) for x in integr_grid]), integr_grid))
    #                 ddata.append(avg)
    #             for val in ddata:
    #                 discrete_data.append(val - discrete_data[0])
    #             return discrete_data
    #         elif self.e:
    #             pass
    #     elif self.o:
    #         if self.w:
    #             pass
    #         elif self.e:
    #             pass
            
    def get_target(self):
        if self.p:
            target_profile = np.loadtxt(self.target.target_data)
            return target_profile
        elif self.d:
            return [float(x) for x in [line.strip() for line in open(self.target.target_data)][0].split(',')]
    
    def calc_improvement(self, orig):
        """Calculates the improvement wrt target quantity
        depending on the type of the target data
        
        Args:
            orig (Mod): the Mod object for the unmodified topology

        Returns: None

        """
        if self.p:
            # need to resample profile for the overlapping region so that x-values match
            target_data = self.get_target()
            minval = np.max([self.profile[0, 0], target_data[0, 0]])
            maxval = np.min([self.profile[-1, 0], target_data[-1, 0]])
            npoints = np.max([len(self.profile), len(target_data)])
            target_profile = interp1d(target_data[:, 0], target_data[:, 1])
            data_profile = interp1d(self.profile[:, 0], self.profile[:, 1])
            self.target.target_data = np.zeros((npoints, 2))
            self.profile = np.zeros((npoints, 2))
            self.target.target_data[:, 0] = np.linspace(minval, maxval, npoints)
            self.target.target_data[:, 1] = np.array([target_profile(x) for x in self.target.target_data[:, 0]])
            self.profile[:, 0] = np.linspace(minval, maxval, npoints)
            self.profile[:, 1] = np.array([data_profile(x) for x in self.profile[:, 0]])
            assert len(self.target.target_data[:, 0]) == len(self.profile[:, 0])
            if self.f:
                # can shift all free energies so that mean is equal to the original data mean
                self.target.target_data[:, 1] -= (np.mean(self.target.target_data[:, 1]) - np.mean(orig.profile[:, 1]))
                self.profile[:, 1] -= (np.mean(self.profile[:, 1]) - np.mean(orig.profile[:, 1]))
            missing_target = self.target.target_data[:, 1] - orig.profile[:, 1]
            diff_wrt_orig_quant = (self.profile[:, 1] - orig.profile[:, 1]) / self.multiplier
            np.savetxt("derivative-{}-{}.txt".format(str(self.mod), self.name),
                       np.vstack((self.profile[:, 0], diff_wrt_orig_quant)).T, fmt='%8.3f')
            smooth = 10
            xrange = self.profile[:, 0]
            smdf = (diff_wrt_orig_quant[smooth-1:] + sum([diff_wrt_orig_quant[smooth-n-1: -n] for n in
                    range(1, smooth-1)]) + diff_wrt_orig_quant[:-(smooth-1)])/smooth
            smxr = (xrange[smooth-1:] + sum([xrange[smooth-n-1: -n] for n in range(1, smooth-1)]) +
                    xrange[:-(smooth-1)])/smooth
            np.savetxt("smooth_derivative-{}-{}.txt".format(str(self.mod), self.name),
                       np.vstack((smxr, smdf)).T, fmt='%8.3f')
            spread_targ = np.max(self.target.target_data[:, 1]) - np.min(self.target.target_data[:, 1])
            spread_orig = np.max(orig.profile[:, 1]) - np.min(orig.profile[:, 1])
            spread = (spread_targ+spread_orig)/2
            # need to avoid division by 0, so simply return 0 if denom is close to 0
            quotient = np.array([int(missing_target[i] > (spread/1000)) * diff_wrt_orig_quant[i]/missing_target[i]
                                 for i in range(len(self.target.target_data))])
            mean_quotient = trapz(quotient, self.target.target_data[:, 0])/(self.target.target_data[-1, 0] -
                                                                            self.target.target_data[0, 0])
            self.sensitivity = mean_quotient
        elif self.d:
            # now we have a discrete set of states, should check whether to shift first to 0
            self.target.target_data = self.get_target()
            missing_target = self.target.target_data - orig.data
            diff_wrt_orig_quant = (self.data - orig.data)/self.multiplier
            self.sensitivity = diff_wrt_orig_quant / missing_target
