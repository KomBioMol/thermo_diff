# ThermoDiff

A Python script designed to facilitate the optimization of force fields based on target quantities and/or free energies

## Getting started

The use of Anaconda is highly encouraged as it contains all necessary libraries. The script itself uses several libraries: numpy, scipy, matplotlib and pandas.

## Available modes

### Possible input options (changes):

Many lines can be contained in a single input file, each input letter (cseadnN) will result in a new rerun being performed

Note that:
+ `c` corresponds to a change in charge
+ `s` corresponds to a change in sigma
+ `e` corresponds to a change in epsilon
+ `a` corresponds to a change in angle force constant
+ `d` corresponds to a change in dihedral force constant
+ `n` corresponds to a change in pairwise sigma (NBFIX)
+ `m` corresponds to a change in pairwise epsilon (NBFIX)

#### change charge/sigma/epsilon:
+ chosen atoms of a single type, all within a single residue (note: need to be of the same type):
```
cse AMFB-1 O11 O12 O13 O6 O7 O9 O10 O1 O2 O16
```
+ chosen atoms of a single type, possibly different residues (note: need to be of the same type):
```
cse AMFB-1-O11 AMFB-1-O12 AMFB-1-O13 AMFB-1-O6 AMFB-1-O7 AMFB-1-O9 AMFB-1-O10 AMFB-1-O1 AMFB-1-O2 AMFB-1-O16
```
+ all atoms of a specified type:
```
cse OG311
```
#### change angle/dihedral parameters:
+ chosen sets of atoms, all within a single residue (possible multiples of 3 (angles) or 4 (dihedrals) in a single line):
```
d AMFB-1 C1 C2 C3 C4 
a AMFB-1 C1 C2 C3 C1 C2 C4
```
+ chosen sets of atoms, possibly different residues (note: need to have the same type signature):
```
d AMFB-1-C1 AMFB-1-C2 AMFB-1-C3 AMFB-1-C4 
a AMFB-1-C1 AMFB-1-C2 AMFB-1-C3 AMFB-1-C1 AMFB-1-C2 AMFB-1-C4
```
+ chosen set of types:
```
a CG311 CG311 OG311
d OG311 CG311 CG311 OG311
```
#### change nbfix sigma/epsilon:
+ chosen sets of atoms, all within a single residue (";"-delimited; need to have consistent types)
```
nm AMFB-1 O11 O12 O13; H1 H2 H3
```
+ chosen sets of atoms, possibly different residues (";"-delimited; need to have consistent types)
```
nm AMFB-1-O11 AMFB-1-O12 AMFB-1-O13; AMFB-1-H1 AMFB-1-H2 AMFB-1-H3
```
+ chosen pair of types:
```
nm HGA2 OG311
```

### Possible input options (target quantities):

Again, many lines can be contained in a single input file, starting with
two letters \[fo\]\[ew\]\[pat\] followed by a user-specified keyword-value
pairs (see below for examples).

Note that:
+ `f` sets free energy as a target
+ `o` sets any other observable (e.g. dihedral, distance, radius of gyration etc.) as a target
+ `e` indicates that data was obtained from equilibrium simulations (no reweighting required;
requires a file with paths to per-frame trajectory data, passed with the `data=...` keyword;
two-column datafiles are required, with the reaction coordinate in the first and the observable
in the second column; if the target is the reaction coordinate itself, the column should be repeated)
+ `w` indicates that data was obtained from umbrella sampling simulations (reweighting required),
requires a metafile for WHAM (consistent with Alan Grossfield's implementation, see below),
passed with the `meta=...` keyword; if the target is an observable (i.e., `o` was set),
the `data=...` keyword also needs to be specified, as WHAM will only provide weights
+ `p` indicates that the reference quantity is a profile (`ref=...` should contain path to a similarly
formatted file with the reference profile)
+ `a` indicates that the reference quantity is an ensemble average (`ref=...` should pass a float
corresponding to the target value)
+ `t` indicates that the reference quantity is a difference between two states (`ref=...` should
contain the target value, and `threshold=...` three sorted values defining the boundaries of the two states)

(the examples below are only sample inputs, i.e., will work with any kind of data)

#### free energy profile as a reference

+ a free energy difference (-5 kcal/mol) between a bound and unbound state, calculated from US with bound state defined at up to 2 nm:
```
fwt label=binding-affinity meta=/path/to/file/metafile.dat ref=-5.0 threshold=1.0,2.0,2.0,2.5
```
+ a free energy profile for the extension of a molecule, calculated from an equilibrium distribution
```
fep label=extension-curve orig=/path/to/file/original_data.dat ref=/path/to/file/target_profile.dat
```

#### user-defined quantity as a reference
+ a difference in dipole moment (3 D) between a bound and unbound state, calculated from an equilibrium distribution (bound state defined at up to 2 nm and unbound from 1.5 to 2.5 nm):
```
oet label=dipole-moment orig=/path/to/file/original_data.dat ref=3.0 threshold=0.5,1.0,1.5,2.5
```
+ an average value of a pseudo-dihedral set to 150 degrees, calculated from an equilibrium distribution:
```
oea label=pseudo-dihedral orig=/path/to/file/original_data.dat ref=150.0
```
+ a profile of a pseudo-dihedral in DNA along an A-to-B conformational transition, calculated from US:
```
owp label=pseudo-dihedral-profile meta=/path/to/file/metafile.dat ref=/path/to/file/target_profile.dat
```