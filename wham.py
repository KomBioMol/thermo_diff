import numpy as np

try:
    from pandas import read_csv
    pandas_avail = True
except ImportError:
    pandas_avail = False
    print("No pandas found, will fall back to the slower numpy-based version")
from scipy.interpolate import interp1d


class Wham:
    # TODO need to rewrite so that the class instance contains all relevant info (weights, Fs etc.)
    """
    Implements the self-consistent WHAM algorithm to calculate
    the free energy profile from Umbrella Sampling simulations
    :param opt:
    """
    def __init__(self, opt):
        # options
        self.opt = opt
        # general settings
        self.nbins = None
        self.p_grid = None
        self.bin_edges = None
        self.nwindows = None
        self.kb_t = 0.001986 * self.opt.t  # kcal/mol
        # actual data and results
        self.pot_interp, self.data, self.centers, self.ks, self.files = None, None, None, None, None
        self.combined = None
        self.weights = None
        self.num = None
        self.p, self.g, self.f = None, None, None
        self.outweights = None
        # error analysis
        self.autocorr = None
        self.boot_framenums = None
        self.gs_boot = None
        self.gs_error = None
        self.precision = np.sqrt(np.finfo(float).eps)
        self(opt.meta)
    
    def __call__(self, meta):
        """
        the main routine for running WHAM with a single metafile,
        wrapped together to enable usage from outside the script
        :param meta: str, metafile to read from
        :return: None
        """
        self.read_data_and_setup_grids(meta)
        self.f, self.g = self.iterate_to_consistency(self.num, self.weights)
        self.f -= self.f[0]
        if self.autocorr:
            self.bootstrap()
    
    def read_data_and_setup_grids(self, meta):
        """
        reads the metafile to determine all initial parameters,
        including colvar bounds, grids, bootstrap flags; also
        reads data into memory
        :param meta: str, metafile to read from
        :return: None
        """
        self.files = [x.strip().split() for x in open(meta)]
        self.check_if_bootstrap(self.files)
        self.nwindows = len(self.files)
        if not all([len(q) == len(self.files[0]) for q in self.files]):
            raise ValueError('not all lines in metafile have the same num of cols, likely an error')
        if pandas_avail:
            self.data = [read_csv(q[0], header=None, delim_whitespace=True).as_matrix()[:, 1] for q in self.files]
        else:
            self.data = [np.loadtxt(q[0])[:, 1] for q in self.files]
        self.combined = np.concatenate(self.data)
        # determine min and max CV vals to specify bounds automatically
        vmin, vmax = np.min(self.combined), np.max(self.combined)
        self.nbins = self.opt.n
        self.weights = self.get_weights(self.files)
        potentials = self.get_pots(self.files, vmin, vmax, self.nbins)
        # setup grids, interpolate potentials and calc histograms
        self.p_grid = np.linspace(vmin, vmax, self.nbins)
        max_v = np.max(np.concatenate([x[:, 1] for x in potentials]))
        self.pot_interp = [interp1d(q[:, 0], q[:, 1], bounds_error=False, fill_value=max_v)(self.p_grid)
                           for q in potentials]
        spacing = self.p_grid[1] - self.p_grid[0]
        self.bin_edges = np.linspace(vmin - spacing / 2, vmax + spacing / 2, self.nbins + 1)
        self.num = np.histogram(self.combined, self.bin_edges, weights=np.concatenate(self.weights))[0]
    
    def get_weights(self, files):
        """
        if you want custom weights for individual frames, just add 3rd col
        to your data files; will be recognized and read by this fn
        :param files: list, paths to data files, as included in the metafile
        :return: list of arrays that contain weights corresponding to individual frames
        """
        contains_weights = np.loadtxt(files[0][0]).shape[1] == 3
        if contains_weights:
            if pandas_avail:
                weights = [read_csv(q[0], header=None, delim_whitespace=True).as_matrix()[:, 2] for q in files]
            else:
                weights = [np.loadtxt(q[0])[:, 2] for q in files]
            weights_cat = np.concatenate(weights)
            weights = [w*weights_cat.shape[0] / np.sum(weights_cat) for w in weights]
        else:
            weights = [np.ones(len(window), dtype=int) for window in self.data]
        return weights
    
    def calc_final_weights(self):
        new_weights = [np.exp(((0.5 * self.ks[w] * (self.data[w] - self.centers[w]) ** 2)-self.f[w])/self.kb_t)
                       for w in range(len(self.data))]
        final_weights = [new * old for new, old in zip(new_weights, self.weights)]
        assert len(self.files) == len(final_weights)
        self.outweights = []
        for weights in final_weights:
            self.outweights.append(weights)
    
    def get_pots(self, files, vmin, vmax, nbins):
        """
        for compatibility with Grossfield's version - 3 cols in the metafile
        imply that data are read as data_file, pot_center, frc_constant
        :param files: list, paths to files with tabulated potentials
        :param vmin: float, beg of interval to consider
        :param vmax: float, end of interval to consider
        :param nbins: int, number of bins
        :return: list, contains np.arrays with tabulated potentials
        """
        try:
            _ = float(files[0][1])
            pots_are_harmonic = True
        except ValueError:
            pots_are_harmonic = False
        if pots_are_harmonic:
            self.centers = [float(q[1]) for q in files]
            self.ks = [float(q[2]) for q in files]
            windows = len(files)
            potentials = [np.vstack([np.linspace(vmin, vmax, nbins),
                                     0.5 * self.ks[w] * (np.linspace(vmin, vmax, nbins) - self.centers[w]) ** 2]).T
                          for w in range(windows)]
        else:
            potentials = [np.loadtxt(q[1]) for q in files]
        return potentials
    
    def check_if_bootstrap(self, files):
        """
        reads metafile, returns False if no bootstrapping required
        or a list of autocorr times (in dataframe units) otherwise
        :return:
        """
        if len(files[0]) == 4:
            acorr = [float(line[3]) for line in files]
        elif len(files[0]) == 3:
            try:
                _ = float(files[0][1])
                acorr = False
            except ValueError:
                acorr = [float(line[2]) for line in files]
        else:
            acorr = False
        self.autocorr = acorr
    
    def get_boot_framenums(self):
        """
        calculates the number of frames to draw from each window while
        bootstrapping, based on autocorrelation time passed in the metafile
        :return: None
        """
        self.boot_framenums = [int(len(self.data[x]) / self.autocorr[x]) for x in range(len(self.data))]
    
    def iterate_to_consistency(self, num, weights):
        """
        iteratively recalculates G and F until self-consistency
        is reached within given tolerance
        :param num: float, numerator in the WHAM eqn
        :param weights: list, data on which to operate, list of np.arrays
        :return: f: np.array holding the F values for each US window
        g: np.array with free energy values along the RC
        """
        self.p = np.ones(self.nbins) / self.nbins
        f = np.zeros(self.nwindows)
        tol = self.opt.tol
        diff = np.infty
        while diff > tol:
            den = np.zeros(len(self.p_grid))
            for j in range(len(self.pot_interp)):
                den += np.sum(weights[j]) * np.exp((f[j] - self.pot_interp[j]) / self.kb_t)
            temp = num / den
            diff = np.max(np.abs(temp - self.p))
            self.p = temp
            for j in range(len(f)):
                f[j] = -self.kb_t * np.log(np.sum(self.p * np.exp(-self.pot_interp[j] / self.kb_t)))
        np.place(self.p, self.p <= 0, self.precision)
        g = -self.kb_t * np.log(self.p) - np.min(-self.kb_t * np.log(self.p))
        return f, g
    
    def bootstrap(self):
        """
        subsamples the original data multiple times based on
        autocorrelation time, then redoes the iterative calculation
        and reports the distribution of results
        :return: None
        """
        n_tries = self.opt.boot_iter
        self.get_boot_framenums()
        gs_boot_temp = []
        for i in range(n_tries):
            if i % 10 == 0:
                print("Bootstrap iteration {}\n".format(i))
            selection = [np.random.choice(range(len(self.data[x])), self.boot_framenums[x], replace=True)
                         for x in range(len(self.data))]
            sel_frames = [self.data[x][selection[x]] for x in range(len(self.data))]
            sel_weights = [self.weights[x][selection[x]] for x in range(len(self.weights))]
            num = np.histogram(np.concatenate(sel_frames), self.bin_edges, weights=np.concatenate(sel_weights))[0]
            _, g = self.iterate_to_consistency(num, sel_weights)
            gs_boot_temp.append(g)
        self.gs_boot = np.vstack(gs_boot_temp)
        self.calc_error()
    
    def calc_error(self):
        """
        calculates the free energy error as the standard deviation
        of free energy profiles obtained from bootstrapping
        :return: None
        """
        mean = np.mean(self.gs_boot, axis=0)
        self.gs_error = np.std([g - mean for g in self.gs_boot], axis=0)
