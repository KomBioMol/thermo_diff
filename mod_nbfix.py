from mod import Mod


class ModNbfix(Mod):
    def __init__(self, top, master, resnr='', resnames='', names='', changes=''):
        """
        Subclass to work with modified NBFIX params (pairwise corrections,
        sigma and epsilon defined for pairs of types).
        The class allows two lists (actually, tuples) of atoms that correspond
        to two atom types (atomtypes have to be consistent within each tuple);
        these atoms' types will be cloned from (type1, type2) to (Xtype1,
        Ytype2), and the corresponding NBFIX parameters will be modified.
        The modification can be applied to all atoms bearing the specific type
        as well as only a subgroup of these.
        :param top: a Top object corresponding to the modified topology
        :param resnr: a list of two tuples, each containing a list of residue numbers
        :param resnames: a list of two tuples, each containing a list of residue names
        :param names: a list of two tuples, each containing a list of atom names
        :param changes: single character denoting the change to be performed
        """
        super(ModNbfix, self).__init__(top, master, resnr, resnames, names)
        self.nbs = 'n' if 'n' in changes else ''
        self.nbe = 'm' if 'm' in changes else ''
        if not (self.nbs or self.nbe):
            raise ValueError("no mode selected appropriate for an ModParam object")
        for t, tnames, tres, tresnames in zip(self.top, self.names, self.res, self.resnames):
            self.LJ_sigmas = []
            self.LJ_epsilons = []
            self.NB_sigmas = None
            self.NB_epsilons = None
            self.types_are_consistent(t, tnames, tres, tresnames)
            if 'nonbond_params' not in t.section_headers:
                self.add_nbfix_section(t)
            try:
                self.types = (self.get_type(t, tnames[0][0], tres[0][0], tresnames[0][0]),
                              self.get_type(t, tnames[1][0], tres[1][0], tresnames[1][0]))
            except IndexError:
                pass
            else:
                self.check_lj_params(t)
                if self.types[0] == self.types[1]:
                    self.prefixes = ['Y', 'Y']
                else:
                    self.prefixes = ['Q', 'Y']
                # we mod the first type
                self.clone_type(t, atomtype=self.types[0], prefix=self.prefixes[0])
                self.prefix_type(t, tnames, tres, tresnames, 0)
                # and then second, if different than first
                if self.types[0] != self.types[1]:
                    self.clone_type(t, atomtype=self.types[1], prefix=self.prefixes[1])
                    self.prefix_type(t, tnames, tres, tresnames, 1)
                self.sort_dihedrals(t)
                self.mod_nb_sigma_eps(t)
    
    def mod_nb_sigma_eps(self, top):
        """
        looks for a line in topology section 'nonbond_params'
        and modifies it as needed
        :return: None
        """
        for section in top.list_sections('nonbond_params'):
            to_remove = []
            for line in range(len(top.sections[section])):
                lspl = top.sections[section][line].split()
                prefixed_types = (self.prefixes[0]+self.types[0], self.prefixes[1]+self.types[1])
                if len(lspl) > 4 and ((lspl[0], lspl[1]) == prefixed_types or
                                      (lspl[1], lspl[0]) == prefixed_types):
                    to_remove.append(top.sections[section][line])
            for redundant in to_remove:
                top.sections[section].remove(redundant)
        section = top.list_sections('nonbond_params')[0]
        top.sections[section].extend(self.increment_nb_sigma_eps_line(self.NB_epsilons is None and
                                                                      self.NB_sigmas is None))
        #
        # for l in range(len(top.sections[section])):
        #     lspl = top.sections[section][l].split()
        #     if len(lspl) > 4 and ((lspl[0] == self.prefixes[0] + self.types[0]
        #                            and lspl[1] == self.prefixes[1] + self.types[1])
        #                           or (lspl[1] == self.prefixes[0] + self.types[0]
        #                               and lspl[0] == self.prefixes[1] + self.types[1])):
        #         modline = self.increment_nb_sigma_eps_line(lspl)
        #         top.sections[section][l] = modline
    
    def increment_nb_sigma_eps_line(self, add_original):
        """
        takes a line from the topology section 'atomtypes'
        and increases the specific value by self.dpar
        :return: an assembled line with sigma modified
        """
        fstring = '  {:<11s}{:<11s}{:>3s}{:>20.8f}{:>20.8f}\n'
        if self.NB_epsilons is None and self.NB_sigmas is None:
            sigma = 0.5*(self.LJ_sigmas[0] + self.LJ_sigmas[1])
            epsilon = (self.LJ_epsilons[0] * self.LJ_epsilons[1])**0.5
        else:
            sigma = self.NB_sigmas
            epsilon = self.NB_epsilons
        cont_list = [self.types[0], self.types[1], '1', sigma, epsilon]
        mod_list = [self.prefixes[0] + self.types[0], self.prefixes[1] + self.types[1], '1']
        if self.nbs:
            mod_list.extend([sigma + self.dpar, epsilon])
        if self.nbe:
            mod_list.extend([sigma, epsilon + self.dpar])
        returnlist = [fstring.format(*cont_list)] if add_original else []
        returnlist.append(fstring.format(*mod_list))
        return returnlist
    
    def prefix_type(self, top, tnames, tres, tresnames, prefix_number):
        """
        looks for a line in topology section 'atoms'
        and modifies it as needed
        :return: None
        """
        for x in range(len(tnames[prefix_number])):
            name = tnames[prefix_number][x]
            resname = tresnames[prefix_number][x]
            resnum = tres[prefix_number][x]
            for section in top.list_sections('atoms'):
                for line in range(len(top.sections[section])):
                    lspl = top.sections[section][line].split()
                    if len(lspl) >= 7 and lspl[4] == name and int(lspl[2]) == int(resnum) \
                            and lspl[3] == resname and lspl[1] == self.types[prefix_number]:
                        modline = self.add_prefix_to_line(lspl, prefix_number)
                        top.sections[section][line] = modline
    
    def add_prefix_to_line(self, cont_list, prefix_number):
        """
        takes a line from the topology section 'atoms'
        and increases charge by self.dpar;
        also changes atomtype to modified (Y-prefixed)
        :param cont_list: line passed as a list of non-whitespace strings
        :param prefix_number: int, which prefix to choose
        :return: an assembled line with type and/or charge modified
        """
        prefix = self.prefixes[prefix_number]
        cont_list[6] = float(cont_list[6])
        if not self.alch:
            cont_list[1] = prefix + cont_list[1]
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}\n'
            if len(cont_list) == 7:
                cont_list = cont_list + [' ']
            return fstring.format(*cont_list[:8])
        else:
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}{:>11s}{:>11.4f}{:>11s}\n'
            if len(cont_list) == 7:
                raise RuntimeError("You need to provide mass for the atom to be changed")
            return fstring.format(*cont_list[:8], prefix + cont_list[1], cont_list[6], cont_list[7])
    
    def types_are_consistent(self, top, tnames, tres, tresnames):
        """
        if many atoms are passed in a single input line, need to make sure
        they are all of the same type; raises ValueError if this is not the case
        :return: None
        """
        consistent1 = all([self.get_type(top, tnames[0][0], tres[0][0], tresnames[0][0])
                          == self.get_type(top, tnames[0][n], tres[0][n], tresnames[0][n])
                          for n in range(len(tnames[0]))])
        consistent2 = all([self.get_type(top, tnames[1][0], tres[1][0], tresnames[1][0])
                           == self.get_type(top, tnames[1][n], tres[1][n], tresnames[1][n])
                           for n in range(len(tnames[1]))])
        if not consistent1 or not consistent2:
            raise ValueError("atoms within a single nbfix modification need to have consistent types")

    @staticmethod
    def add_nbfix_section(top):
        position = top.section_headers.index('moleculetype')
        top.section_headers.insert(position, 'nonbond_params')
        top.sections.insert(position, ['; NBFIX  rmin=<charmm_rmin>/2^(1/6), eps=4.184*<charmm_eps>\n',
                                       ';name   type1  type2  1  sigma   epsilon\n', '[ nonbond_params ]\n'])

    def check_lj_params(self, top):
        for section in top.list_sections('atomtypes'):
            for line in range(len(top.sections[section])):
                lspl = top.sections[section][line].split()
                if len(lspl) > 6 and lspl[0] in self.types:
                    self.LJ_sigmas.append(float(lspl[5]))
                    self.LJ_epsilons.append(float(lspl[6]))
        if len(self.LJ_sigmas) == 2 and len(self.LJ_epsilons) == 2:
            pass
        elif len(self.LJ_sigmas) == 1 and len(self.LJ_epsilons) == 1 and self.types[0] == self.types[1]:
            self.LJ_sigmas.append(self.LJ_sigmas[0])
            self.LJ_epsilons.append(self.LJ_epsilons[0])
        else:
            raise RuntimeError("Found {} LJ sigmas and {} LJ epsilons for types {}, "
                               "check your topology".format(len(self.LJ_sigmas), len(self.LJ_sigmas), self.types))
        for section in top.list_sections('nonbond_params'):
            for line in range(len(top.sections[section])):
                lspl = top.sections[section][line].split()
                if len(lspl) > 4 and ((lspl[0], lspl[1]) == (self.types[0], self.types[1]) or
                                      (lspl[0], lspl[1]) == (self.types[1], self.types[0])):
                    self.NB_sigmas = float(lspl[3])
                    self.NB_epsilons = float(lspl[4])
