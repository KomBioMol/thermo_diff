import os


class Top:
    def __init__(self, filename, gmx_dir='/usr/share/gromacs/top/'):
        """
        A class to represent and contain the Gromacs topology file and provide
        tools for editing topology elements
        :param filename: str, path to the .top file
        :param gmx_dir: str, Gromacs FF directory
        """
        self.fname = filename
        self.top = self.fname.split('/')[-1]
        self.dir = '/' + '/'.join(self.fname.split('/')[:-1])
        self.contents = open(self.fname).readlines()
        self.gromacs_dir = gmx_dir
        self.pref = ''
        self.include_all()
        try:
            self.section_headers = [line.split()[1].strip(']').strip() for line in self.contents
                                    if line.strip().startswith('[')]
        except IndexError:
            self.section_headers = [line.strip().strip('[]').strip() for line in self.contents
                                    if line.strip().startswith('[')]
        self.sections = self.get_sections()
        self.mols = self.def_molecules()
        self.names_to_nums, self.nums_to_types, self.nums_to_names = None, None, None
    
    def get_dicts(self):
        """
        dicts are not always needed and are costly to calculate,
        so only fill in the values when explicitly asked to
        :return: None
        """
        if not all([self.names_to_nums, self.nums_to_types, self.nums_to_names]):
            self.names_to_nums, self.nums_to_types, self.nums_to_names = self.mol_type_nums()
    
    def get_section(self, ptype):
        order = {'m': 't', 't': 'b', 'b': 'p', 'p': 'a', 'a': 'd', 'd': 'i'}
        if ptype in 'mtbpad':
            return [l for l in self.contents[self.get_bos(ptype):self.get_bos(order[ptype])] if not l.isspace()]
        elif ptype == 'i':
            return [line for line in self.contents[self.get_bos(ptype):] if line.startswith('[ dih')
                    or line.startswith(';  ai') or (len(line.split()) > 4 and line.split()[4] == '4')]
        elif ptype == 'h':
            return self.contents[:self.get_bos('m')]
        elif ptype == 'f':
            term_line = self.contents.index(self.get_section('i')[-1]) + 1
            return self.contents[term_line:]
    
    def get_bos(self, ptype):
        """
        finds line num in topology on which the respective section begins
        :param ptype: str, parameter type (section name)
        :return: int, requested line number
        """
        lines = [i for i in range(len(self.contents)) if len(self.contents[i].split()) > 2
                 and self.contents[i].split()[1] == self.sections[ptype]]
        if ptype in 'mtbpad':
            return lines[0]
        elif ptype == 'i':
            return lines[1]
    
    def include_all(self):
        """
        includes all .itp files in the .top file to facilitate processing
        :return: None
        """
        lines = [i for i in range(len(self.contents)) if self.contents[i].startswith("#include")]
        while len(lines) > 0:
            lnum = lines[0]
            to_include, pref = self.find_in_path(self.contents.pop(lnum).split()[1].strip('"\''))
            contents = open(to_include).readlines()
            self.contents[lnum:lnum] = self.parse_include(contents, pref)
            lines = [i for i in range(len(self.contents)) if self.contents[i].startswith("#include")]
    
    @staticmethod
    def parse_include(content, prefix):
        if prefix:
            for lnum, line in enumerate(content):
                if line.strip().startswith('#include'):
                    content[lnum] = '#include "{}/{}"'.format(prefix, line.strip().split()[1].strip("'\""))
        return content
    
    def find_in_path(self, filename):
        """
        looks for a file to be included in either the current directory
        or in Gromacs directories (as given by user), in order to
        include all .itp files in a single .top file
        :param filename: str, name of the file to be searched for
        :return: None
        """
        pref = ''  # TODO fix
        if filename in os.listdir(self.dir):
            return self.dir + '/' + filename, ''
        else:
            # filename can have 4 forms: regular "file.itp", full relative "./file.itp",
            # relative with .. "../../topol/file.itp", or absolute "/path/to/file.itp"
            search_in_gmx_dir = True
            relative = False
            if filename.startswith('./'):
                filename = filename
                search_in_gmx_dir = False
                relative = True
            elif filename.startswith('..'):
                search_in_gmx_dir = False
                relative = True
            elif filename.startswith('/'):
                search_in_gmx_dir = False
            else:
                relative = True
            suff = filename.split('/')[-1]
            pref = '/'.join(filename.split('/')[:-1])
            if not relative:
                if suff in os.listdir(pref):
                    return filename, pref
            else:
                full_filename = self.dir + '/' + filename
                full_pref = self.dir + '/' + pref
                if suff in os.listdir(full_pref):
                    return full_filename, full_pref
                elif suff in self.gromacs_dir + '/' + pref and search_in_gmx_dir:
                    return self.gromacs_dir + '/' + filename, self.gromacs_dir + '/' + pref
        raise ValueError('file {} not found in neither local nor Gromacs directory'.format(filename))
    
    def get_sections(self):
        sections = []
        section_startlines = [0]
        headers_stack = self.section_headers[:]
        for l in range(len(self.contents)):
            line = self.contents[l].strip()
            if headers_stack and headers_stack[0] in line and line.startswith('['):
                section_startlines.append(l)
                headers_stack.pop(0)
        section_startlines.append(len(self.contents))
        self.section_headers.insert(0, 'header')
        for s in range(len(section_startlines)-1):
            sections.append(self.contents[section_startlines[s]:section_startlines[s+1]])
        return sections
            
    def list_sections(self, section_name):
        return [i for i in range(len(self.section_headers)) if self.section_headers[i] == section_name]
    
    def def_molecules(self):
        """
        Extracts individual molecules from topology, creating a dict that binds
        the moleculename to a list of numbers of sections that contain its bonded params
        :return: dict containing molname:[section_numbers] bindings
        """
        mols = {}
        moltypes = self.list_sections('moleculetype')
        t, b, p, a, d = [self.list_sections(x) for x in ['atoms', 'bonds', 'pairs', 'angles', 'dihedrals']]
        bondeds = t + b + p + a + d
        moltypes.append(len(self.sections)-1)
        for m, mnext in zip(moltypes[:-1], moltypes[1:]):
            sect = self.sections[m]
            molname = [x.split()[0] for x in sect if len(x.split()) > 1
                       and not x.lstrip().startswith('[') and not x.lstrip().startswith(';')][0]
            mols[molname] = [x for x in bondeds if m < x < mnext]
            mols[molname].sort()
        return mols
    
    def mol_type_nums(self):
        """
        Provides bindings between atomnumber and atomtype
        and vice versa for each molecule identified in
        the topology
        :return: dict containing molname:(type:num) and
        molname:(num:type) bindings
        """
        names_to_nums = {}
        nums_to_names = {}
        nums_to_types = {}
        for mol in self.mols.keys():
            names_to_nums[mol] = {}
            nums_to_types[mol] = {}
            nums_to_names[mol] = {}
            atoms_section = self.mols[mol][0]
            for line in self.sections[atoms_section]:
                lspl = line.split()
                if len(lspl) > 7 and line.lstrip()[0] not in ["'", '[', ';']:
                    names_to_nums[mol]["{}-{}-{}".format(lspl[3], lspl[2], lspl[4])] = lspl[0]
                    nums_to_names[mol][lspl[0]] = "{}-{}-{}".format(lspl[3], lspl[2], lspl[4])
                    nums_to_types[mol][lspl[0]] = lspl[1]
        return names_to_nums, nums_to_types, nums_to_names
    
    def find_type(self, atomtype):
        sections = self.list_sections('atoms')
        results = []
        for s in sections:
            for l in range(len(self.sections[s])):
                lspl = self.sections[s][l].split()
                if len(lspl) > 7 and lspl[1] == atomtype:
                    results.append('{}-{}-{}'.format(lspl[3], lspl[2], lspl[4]))
        return results
    
    def save_mod(self, outname):
        with open(outname, 'w') as outfile:
            for section in self.sections:
                for line in section:
                    outfile.write(line)
                outfile.write('\n')
        try:
            from gromologist import Top
        except ModuleNotFoundError:
            print("Couldn't add FF params, please install gromologist from gitlab.com/KomBioMol/gromologist")
        else:
            t = Top(outname)
            t.add_ff_params()
            t.save_top(outname)


if __name__ == "__main__":
    top = Top('topol.top')
