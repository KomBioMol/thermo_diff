from optparse import OptionParser
import matplotlib.pyplot as plt
from itertools import combinations
from skimage.filters import gaussian

from mod import Mod
from processing import *
from top import Top
from quantity import Quantity

# TODO's (sorted by priority):
# - test with at least 2 targets
# - calc final combinations of Mods to match target & make plots
# - enable each traj to have different temperature (separate Traj obj, with reference to top, temperature and path)
# - allow to only use # of Mods with highest sensitivity
# - make sensitivity for free energy profiles equiv to the derivative of KL divergence
# - think of multiple dihedrals


class ThermoDiff:
    """
    Main class of the thermo_diff module
    """
    def __init__(self, options):
        self.wdir = os.getcwd()
        self.topol = [self.abs_path(x) for x in options.topol.split()]  # TODO should crash if topols have identical names
        self.struct = [self.abs_path(x) for x in options.struct.split()]
        self.list_mods = self.abs_path(options.list_mods)
        # need a bit of workaround in case trajs_list file is in a different directory than the actual trajectories
        self.trajs_list = self.abs_path(options.trajs_list)
        self.targets_file = self.abs_path(options.target)
        self.gmx = options.gmx
        self.alch = not options.noalch
        self.gmx_dir = options.gmx_dir
        # TODO get rid of kt or move to targets?
        self.kt = options.kt
        self.debug = True if options.debug is True else False
        self.dyn_mdp = self.abs_path(options.dyn_mdp)
        self.nproc = options.nproc
        self.jobs = []
        self.max_opt = options.max_opt
        self.calc_sens = False if options.deriv_only else True
        # better to initialize attributes in __init__
        self.mods, self.trajs, self.coeffs, self.residual, self.sensitivities = None, None, None, None, None

    def do_reruns(self):
        """
        Starts reruns for all trajs and calculates new weights
        Returns: None
        """
        run_parallel(self.nproc, self.jobs, self.debug)
    
    def read_inputs(self):
        """
        Reads input files; now parsing is separated from execution
        st the validity of input can be checked beforehand
        Returns: None
        """
        self.mods = self.initialize_mods()
        targets, trajs = process_targets(self.targets_file, self.trajs_list, self)
        try_mkdir('working')
        os.chdir('./working')
        for m in self.mods:
            name = str(m)
            try_mkdir(name)
            m.save_mod("{}/{}/".format(os.getcwd(), name), name)
            m.trajs = deepcopy(trajs)
            for traj in m.trajs:
                traj.mod = m
                traj.topology = m.top[traj.top_num]
                self.jobs.append((self.struct, self.dyn_mdp, traj, self.gmx, self.alch))
        for m in self.mods:
            m.goto_mydir()
            for t in targets:
                m.qs.append(Quantity(deepcopy(t), m))

    def get_results(self):
        """
        Collects data after reruns, calculates improvements
        wrt pre-defined targets
        Returns: None
        """
        for m in self.mods:
            m.goto_mydir()
            for q in m.qs:
                q.calc_perframe_weights()
                q.calc_perframe_derivatives()
                q.get_perframe_observables()
                q.calc_all_profiles()
                q.write_derivatives()
        if self.calc_sens:
            self.calc_sensitivities()

    def calc_sensitivities(self):
        """
        Calculates the sensitivity matrix, uses it to calculate
        the list of suggested parameter modifications
        Returns: None
        """
        n_mods = len(self.mods) - 1
        n_targets = len(self.mods[1].qs)
        self.sensitivities = np.matrix(np.zeros((n_mods, n_targets)))
        for mm, m in enumerate(self.mods[1:]):
            for qq, q in enumerate(m.qs):
                self.sensitivities[mm, qq] = q.sensitivity
        print("sensitivity matrix:")
        print(self.sensitivities)
        y = np.matrix(np.ones(n_targets)).reshape((n_targets, 1))
        if self.max_opt >= n_mods:
            self.coeffs = (self.sensitivities.T * self.sensitivities).I * self.sensitivities * y.T
            self.residual = np.linalg.norm(y - self.sensitivities.T * self.coeffs)
        else:
            self.residual = np.inf
            for comb in combinations(range(n_mods), self.max_opt):
                y = y[comb]
                sens = self.sensitivities[:, comb]
                coeffs = (sens.T * sens).I * sens * y.T  # TODO check this
                residual = np.linalg.norm(y - sens.T * coeffs)
                if residual < self.residual:
                    self.residual = residual
                    self.coeffs = coeffs
        print("coeffs: {}\n with residual {}\n".format(' '.join(self.coeffs), self.residual))
    
    def show_results(self):
        """
        Plots results on a grid, currently unsupported
        Returns: None
        """
        rows = len(self.mods[-1].qs)
        cols = len(self.mods)
        f, a = plt.subplots(rows, cols)
        for nm, m in enumerate(self.mods):
            for nq, q in enumerate(m.qs):
                extrapolated_profile = (q.profile + q.derivative - np.nanmean(q.derivative)).reshape(-1, 1)
                extrapolated_profile = gaussian(extrapolated_profile, 2)
                a[nq, nm].plot(q.grid, extrapolated_profile)
        plt.savefig('results.svg')
                
        # fig, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(15, 10))
        # ax = [ax] if n_targets == 1 else ax
        # for a in range(n_targets):
        #     plot_results(self.mods, a, ax[a])
        # plt.savefig('results.svg')

    def abs_path(self, path):
        """
        Appends absolute path to a file
        Args:
            path: str, relative path to the file
        Returns: str, absolute path to the file
        """
        if path.startswith('/'):
            return path
        else:
            return self.wdir + '/' + path
    
    def initialize_mods(self):
        """
        Creates Top instances, matches them and initializes
        to individual Mod instances that represent sampled
        modifications of the force field
        Returns: list, list of Mod instances
        """
        top = [Top(t, self.gmx_dir) for t in self.topol]
        mods = [x.strip() for x in open(self.list_mods)]
        if self.alch:
            mods_list = []
        else:
            mods_list = [Mod(deepcopy(top), self)]  # first one goes unmodified as reference
        for m in mods:
            mods_list.extend(parse_input_line(m, deepcopy(top), self))
        return mods_list

    def calc_predictions(self):
        pass


def parse_all():
    parser = OptionParser(usage="")
    parser.add_option("-p", dest="topol", action="store", type="string",
                      help="Topology file")
    parser.add_option("-m", dest="list_mods", action="store", type="string",
                      help="File containing a list of desired modifications, see README for format spec")
    parser.add_option("-c", dest="struct", action="store", type="string",
                      help="Structure file")
    parser.add_option("-f", dest="trajs_list", action="store", type="string",
                      help="File containing a list of trajectories to be analyzed")
    parser.add_option("-d", dest="dyn_mdp", action="store", type="string", default="md.mdp",
                      help="An .mdp file for rerun")
    parser.add_option("-x", dest="gmx", action="store", type="string", default="gmx",
                      help="GROMACS executable")
    parser.add_option("-g", dest="gmx_dir", action="store", type="string", default="/usr/share/gromacs/top",
                      help="Path to GMX ff files")
    parser.add_option("-T", dest="kt", action="store", type="float", default=300,
                      help="Simulation temperature")
    parser.add_option("-n", dest="nproc", action="store", type="int", default=1,
                      help="Number of processes to launch")
    parser.add_option("-q", dest="target", action="store", type="string", default="free",
                      help="File with directives concerning reference quantities (see documentation)")
    parser.add_option("-z", dest="max_opt", action="store", type="int", default="3",
                      help="Max number of parameter changes included in the final prediction, default is 3")
    parser.add_option("-b", dest='debug', action='store_true',
                      help='set to run in debug mode (residual files are not deleted)')
    parser.add_option("-X", dest='check_only', action='store_true',
                      help='only check inputs without actually doing the reruns (useful when running on cluster)')
    parser.add_option("--noalch", dest='noalch', action='store_true',
                      help='do not use the alchemical variant of TD')
    parser.add_option("-D", dest='deriv_only', action='store_true',
                      help='only output derivatives (do not calculate the sensitivity matrix)')
    (options, args) = parser.parse_args()
    options.kt *= 0.008314
    return options


if __name__ == "__main__":
    opt = parse_all()
    td = ThermoDiff(opt)
    td.read_inputs()
    td.do_reruns()
    td.get_results()
    # td.show_results()
