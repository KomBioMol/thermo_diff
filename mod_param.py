from mod import Mod


class ModParam(Mod):
    def __init__(self, top, master, resnr='', resnames='', names='', changes=''):
        """
        Subclass to work with modified bonded params (angles and dihedrals).
        The class allows a lists of tuples of atoms that each corresponds
        to a single bonded term, angle or dihedral (all need to have
        the same sets of atomtypes for the calculation to be meaningful);
        the corresponding parameters will be modified in the molecule definition.
        The modification can be applied to all atoms bearing the specific type
        as well as only a subgroup of these.
        :param top: Top, a Top object corresponding to the modified topology
        :param resnr: list, a list of tuples, each containing a list of 3 or 4 residue numbers
        :param resnames: list, a list of tuples, each containing a list of 3 or 4 residue names
        :param names: list, a list of tuples, each containing a list of 3 or 4 atom names
        :param changes: str, single character denoting the change to be performed
        """
        super(ModParam, self).__init__(top, master, resnr, resnames, names)
        self.dih = 'd' if 'd' in changes else ''  # implement add to top line
        self.ang = 'a' if 'a' in changes else ''  # implement add to top line
        if self.dih and self.ang:
            raise RuntimeError("Please use separate Mods entries for angles and dihedrals")
        if self.dih and not self.ang:
            if len(changes) == 2:
                self.period = changes[1]
            else:
                self.period = ''
        self.npar = 3 if self.ang else 4
        if not (self.dih or self.ang):
            raise ValueError("no mode selected appropriate for an ModParam object")
        for t in self.top:
            self.types_are_consistent(t)
            self.types = tuple(self.get_type(t, n, r, rn) for n, r, rn in
                               zip(self.names[0], self.res[0], self.resnames[0]))
            self.origpar = []
            self.is_wildcard = False
            if self.ang:
                self.find_ang(t)
                self.mod_ang(t)
            if self.dih:
                self.find_dih(t)
                self.mod_dih(t)

    def mod_ang(self, top):
        """
        looks for lines in topology section 'angles'
        and modifies them as needed
        :return: None
        """
        sections = top.list_sections('angles')
        assert len(self.types) == 3
        for s in sections:
            molname = [x for x in top.mols.keys() if s in top.mols[x]][0]
            for l in range(len(top.sections[s])):
                lspl = top.sections[s][l].split()
                if len(lspl) > 3 and lspl[0][0] not in '[;':
                    try:
                        line_nums = [tuple(top.names_to_nums[molname]["{}-{}-{}".format(xr, xrn, xn)]
                                           for xr, xrn, xn in zip(r, rn, n))
                                     for r, rn, n in zip(self.resnames, self.res, self.names)]
                        if any([self.check_line(lspl[:3], line) for line in line_nums]):
                            modline = self.increment_angle(lspl)
                            top.sections[s][l] = modline
                    except KeyError:
                        pass

    def increment_angle(self, cont_list):
        """
        adds the modified angle parameters to specified line
        from topology section 'angles'
        :param cont_list: list, line to be modified, passed as a list of non-whitespace strings
        :return: str, modified and formatted line
        """
        assert len(self.origpar) == 1
        cont_list.extend(self.origpar[0]) # TODO fix
        fstring = '{:>5s} {:>5s} {:>5s} {:>5s}' + len(self.origpar[0]) * '{:>14} '
        return fstring.format(*cont_list)

    def mod_dih(self, top):
        """
        looks for lines in topology section 'dihedrals'
        and modifies them as needed
        :return: None
        """
        sections = top.list_sections('dihedrals')
        assert len(self.types) == 4
        for s in sections:
            molname = [x for x in top.mols.keys() if s in top.mols[x]][0]
            for l in range(len(top.sections[s])):
                lspl = top.sections[s][l].split()
                if len(lspl) > 4 and lspl[0][0] not in '[;':
                    try:
                        line_nums = [tuple(top.names_to_nums[molname]["{}-{}-{}".format(xr, xrn, xn)]
                                           for xr, xrn, xn in zip(r, rn, n))
                                     for r, rn, n in zip(self.resnames, self.res, self.names)]
                        if any([self.check_line(lspl[:4], line) for line in line_nums]):
                            modline = self.increment_dihedral(lspl)
                            top.sections[s][l] = modline
                    except KeyError:
                        pass

    def increment_dihedral(self, cont_list):
        """
        adds the modified dihedral parameters to specified line
        from topology section 'dihedrals'
        :param cont_list: list, line to be modified, passed as a list of non-whitespace strings
        :return: str, modified and formatted line
        """
        assert self.origpar
        result = ''
        fstring = '{:>5s} {:>5s} {:>5s} {:>5s} {:>5s}' + 6 * '{:>14} ' + '\n'
        for par in self.origpar:
            cont_temp = cont_list[:]
            opar = par[:]
            if (self.period and par[2] == self.period) or not self.period:
                par[1] = float(par[1]) + self.dpar
            cont_temp.extend(opar[:3] + par[:3])
            result += fstring.format(*cont_temp)
        return result

    def check_line(self, list_line_to_check, list_entries):
        """
        checks if two lines share the same parameter entries
        (possibly in reverse order, and including wildcards)
        :param list_line_to_check: list, line from topology file split at whitespaces
        :param list_entries: list, contains desired atomtypes as strings
        :return: bool, True if match found
        """
        n = len(list_entries)
        ok = all([self.eq_x(list_line_to_check[i], list_entries[i]) for i in range(n)]) \
            or all([self.eq_x(list_line_to_check[i], list_entries[n - i - 1]) for i in range(n)])
        return ok

    def find_ang(self, top):
        """
        looks for the desired angle parameters in the topology
        section "angletypes"
        :return: None
        """
        section = top.list_sections('angletypes')[0]
        for l in range(len(top.sections[section])):
            lspl = top.sections[section][l].split()
            if len(lspl) > 3 and lspl[0][0] not in '[;' and self.check_line(lspl, self.types):
                self.origpar.append(lspl[4:])
                self.origpar[-1][1] = float(self.origpar[-1][1]) + self.dpar

    def find_dih(self, top):
        """
        looks for the desired dihedral parameters in the topology
        section "dihedraltypes"; also accounts for cases in which
        wildcarded and non-wildcard params can match the request,
        overwriting wildcards with non-wildcards whenever possible
        and saving wildcards when no alternatives exist
        :return: None
        """
        sections = top.list_sections('dihedraltypes')
        for s in sections:
            for l in range(len(top.sections[s])):
                lspl = top.sections[s][l].split()
                if len(lspl) > 6 and lspl[0][0] not in '[;' and self.check_line(lspl, self.types) \
                        and (not self.period or lspl[7] == self.period):
                    if ('X' in lspl[:4] and not self.origpar) or self.is_wildcard:
                        self.origpar.append(lspl[5:])
                        self.is_wildcard = True
                    else:
                        self.origpar.append(lspl[5:])
        if not self.period:
            if not len(self.origpar) == 1:
                raise RuntimeError('Found multiple dihedral entries for the requested modification. To work with '
                                   'multiple dihedral entries, use notation "d1", "d2" etc. where 1, 2, ... denotes'
                                   ' the periodicity of the given term.')

    def types_are_consistent(self, top):
        """
        checks whether all input atoms share the same type signature,
        (t1 t2 t3 t4) for dihedrals or (t1 t2 t3) for angles;
        raises ValueError if not the case
        :return: None
        """
        types_list = [tuple(self.get_type(top, qn, qrn, qr) for qn, qrn, qr in zip(n, rn, r))
                      for n, rn, r in zip(self.names, self.res, self.resnames)]
        ok = all([self.check_line(types_list[0], types_list[n]) for n in range(1, len(types_list))])
        if not ok:
            raise ValueError("atoms within a single parameter modification need to have consistent types")

    @staticmethod
    def eq_x(one, another):
        """
        compares the identity of two types, allowing for wildcards
        ("X" can match any type in the "dihedraltypes" section)
        :param one: str, first element to compare
        :param another: str, second element to compare
        :return: bool, True if match
        """
        if one == another or one == "X" or another == "X":
            return True
        else:
            return False
