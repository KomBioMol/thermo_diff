import numpy as np
try:
    from pandas import read_csv
    pandas_avail = True
except ImportError:
    pandas_avail = False
    print("No pandas found, will fall back to the slower numpy-based version")
pandas_avail = False

class Binner:
    def __init__(self, options):
        """
        options should have the following attributes:
        opt.target (Target instance)
        opt.beg/end/inc (optional)
        opt.wei ("Umbrella" for WHAM-based weighting or "User" for precomputed weights)
        opt.centers/Fi/frccnnst for WHAM-based weighting
        opt.uwei (list of np.arrays) for precomputed weights
        opt.dstenr (str, path to file with datafile paths)
        :param options:
        """
        self.opt = options
        self.data = None
        self.weights, self.weights_working = None, None
        self.bins = []
        self.profile = []
        self.stddev = []
        self.kB = 0.001987  # Boltzmann's constant [kcal/molK]
        self.read_data()
        self.do_calc()
        self.bins, self.profile = np.array(self.bins), np.array(self.profile)
        
    def read_data(self):
        file_list = open(self.opt.dstenr, "r") if isinstance(self.opt.dstenr, str) else self.opt.dstenr
        abs_path = '/'.join(self.opt.dstenr.split('/')[:-1])
        dist_eners = []
        for des in file_list:
            if des.startswith('/'):
                dist_eners.append(des.split()[0])
            else:
                dist_eners.append(abs_path + '/' + des.split()[0])
        if pandas_avail:
            alldata = [read_csv(f, header=None, delim_whitespace=True, comment = ["@", "&"]).as_matrix() for f in dist_eners]
        else:
            alldata = [np.loadtxt(f, comments = ["@", "&"])[:,1].reshape(-1,1) for f in dist_eners]
        self.data = np.concatenate(alldata)
        self.get_ranges()
        self.weights = self.get_weights(alldata)
        #self.get_weights2()
        #self.weights_working = np.concatenate(self.weights).reshape(-1)
    
    def get_ranges(self):
        if self.opt.quantity.p:
            assert self.opt.quantity.target.threshold is None
            self.opt.beg = np.min(self.data[:, 0])
            self.opt.end = np.max(self.data[:, 0])
            self.opt.inc = (self.opt.end - self.opt.beg) / 100
            self.opt.edges = [self.opt.beg - 0.5*self.opt.inc] \
                + [self.opt.beg + (i+0.5)*self.opt.inc for i in range(100)]
        else:  # TODO pass thresholds correctly !!!!!!!!!!!!
            if self.opt.quantity.target.threshold is not None and self.opt.quantity.target.threshold != 'auto':
                self.opt.edges = [-np.inf] + self.opt.quantity.target.threshold + [np.inf]
            elif self.opt.quantity.target.threshold == 'auto':
                vals = sorted(list(set(self.data[:, 0])))
                self.opt.edges = [vals[0] - 0.5 * (vals[1] - vals[0])] \
                    + [0.5 * (vals[x + 1] - vals[x]) for x in range(len(vals) - 1)] \
                    + [vals[-1] + 0.5 * (vals[-1] - vals[-2])]
            else:
                self.opt.edges = [np.min(self.data[:, 0]), np.max(self.data[:, 0])]
        
    def get_weights(self, alldata):
        if self.opt.wei == "Umbrella":
            clist = open(self.opt.centres, "r") if isinstance(self.opt.centres, str) else self.opt.centres
            centre = [float(distances.split()[0]) for distances in clist]
            flist = open(self.opt.Fi, "r") if isinstance(self.opt.Fi, str) else self.opt.Fi
            fis = [float(frees.split()[0]) for frees in flist]
            klist = open(self.opt.frccnst, "r") if isinstance(self.opt.frccnst, str) else self.opt.frccnst
            frccnst = [float(cnst.split()[0]) for cnst in klist]
            dists = [matr[:, 0] for matr in alldata]
            allweig = [np.exp(-(F - 0.5 * k * np.power(x - x0, 2)) / (self.kB * float(self.opt.tempr)))
                       for x, x0, F, k in zip(dists, centre, fis, frccnst)]
            return allweig
        elif self.opt.wei == "User":
            allweig = self.opt.uwei
            allweig_array = np.concatenate(allweig)
            if self.data.shape[0] == allweig_array.shape[0]:
                return allweig_array
            else:
                raise ValueError("Number of user-specified weights does not match the number of data points")
        else:
            raise ValueError("Unrecognized option for weights: {}".format(self.opt.wei))
    
    def get_weights2(self):
        if self.opt.secw:
            allweig2 = self.opt.secw
            for n, w in enumerate(self.weights):
                if len(w) == len(allweig2[n]):
                    self.weights[n] = self.multiply_large(w, allweig2[n])
                else:
                    raise ValueError("Number of user-specified weights for second dimension "
                                     "does not match the number of data points")
    
    def do_calc(self):
        for b_low, b_high in zip(self.opt.edges[:-1], self.opt.edges[1:]):
            logical = np.logical_and(self.data[:, 0] >= b_low,
                                     self.data[:, 0] < b_high)
            working = self.data[logical, 1:]
            weights = self.weights[logical]
            if len(working) > 0:
                self.calc1d(working, weights, 0.5*(b_low + b_high))
    
    def calc1d(self, working, weights, bins):
        denominator = float(np.sum(weights))
        weighted = np.multiply(working, weights[:, np.newaxis])
        sums = np.sum(weighted, 0)
        avgs = sums / denominator
        self.bins.append(bins)
        if self.opt.free: ## TODO check
            self.profile.append(-self.kB * self.opt.tempr * np.log(denominator))
        else:
            self.profile.append(avgs)
        devs_sqrd = np.power(working - avgs, 2)
        weighted_devs_sqrd = np.multiply(devs_sqrd, weights[:, np.newaxis])
        sum_devs = np.sum(weighted_devs_sqrd, 0)
        devs = np.power(sum_devs / denominator, 0.5)
        self.stddev.append("{:10.5f} ".format(bins) + " ".join(["{:10.5f}".format(dev) for dev in devs]))
    
    @staticmethod
    def multiply_large(arr1, arr2):
        try:
            prod = arr1 * arr2
        except MemoryError:
            prod = 0*arr1
            print(arr1.shape, arr2.shape)
            chunks = int(len(arr1)/1000)
            for i in range(chunks):
                print(arr2[1000*i:1000*(i+1)].shape, arr1[1000*i:1000*(i+1)].shape)
                prod[1000*i:1000*(i+1)] = arr1[1000*i:1000*(i+1)] * arr2[1000*i:1000*(i+1)]
            prod[1000*chunks:] = arr1[1000*chunks:] * arr2[1000*chunks:]
        return prod
