import subprocess
import os
import sys
from time import  sleep
import numpy as np
from multiprocessing import Pool
from concurrent.futures import ProcessPoolExecutor
from copy import deepcopy


from mod_atom import ModAtom
from mod_param import ModParam
from mod_nbfix import ModNbfix
from traj import Traj, Trajset

"""
Contains auxillary functions and classes for input parsing, running reruns, processing of files etc.

Functions included:
    run_gromacs
    get_pot_code
    cleanup
    save_weights
    read_energy
    try_mkdir
    run_parallel
    read_free
    read_quantity
    parse_input_line
    check_mod
    process_targets
    process_target_line
    process_trajs
    append_abs_path
    trim_traj
    read_traj_params
    check_line
    plot_results

Classes included:
    Target
    Traj
    OptParserEmul

"""


def run_gromacs(args):
    """
    creates a .tpr file and performs rerun for the specified trajectory
    and a given ModAtom object, running gmx energy afterwards; executed
    in a parallel process
    :param args: a tuple of parameters from the stack
    """
    structs_list, dyn_mdp, traj, gmx, alch = args  # unpack sim parameters
    m = traj.mod
    folder_name = '/'.join(m.topname.split('/')[:-1])
    if alch:
        dyn_mdp = '../../thermo_diff/alch.mdp'
    os.chdir(folder_name)
    pot_code = '8'  # this is default, will get overridden below
    traj_path = traj.path
    traj_alias = traj.ordering
    traj.mod = m
    top_path = m.topname + str(m) + '-' + traj.topology.top
    top_core_name = top_path.split('/')[-1].split('.')[0]
    struct = structs_list[traj.top_num]
    tpr = top_core_name + '-rerun.tpr'
    if tpr not in os.listdir(folder_name):
        subprocess.call('{gmx} grompp -f {mdp} -p {top} -c {struct} -maxwarn 50 '
                        '-o {fname}/{tpr} >> rerun.log 2>&1'.format(mdp=dyn_mdp, top=top_path, gmx=gmx, struct=struct,
                                                                    fname=folder_name, tpr=tpr), shell=True)
    if not alch:
        if '{tn}-energy.xvg'.format(tn=traj_alias) not in os.listdir(folder_name):
            if tpr not in os.listdir(folder_name):
                raise RuntimeError('{}-rerun.tpr not found, aborting (check your output)'.format(top_core_name))
            print("calculating mod {fname}, trajectory {tn}".format(fname=folder_name, tn=traj_alias))
            subprocess.call('{gmx} mdrun -s {fname}/{tpr} -rerun {traj} -ntomp 1 -ntmpi 1 '
                            '-e {fname}/{tn}-ener >> rerun.log 2>&1'.format(gmx=gmx, fname=folder_name, traj=traj_path,
                                                                            tn=traj_alias, tpr=tpr), shell=True)
            energy = 'energy' if gmx else 'g_energy'
            try:  # need to do this to check number code for entry "Potential"
                subprocess.check_output('echo "0" | {gmx} {en} '
                                        '-f {fname}/{tn}-ener'.format(gmx=gmx, fname=folder_name, traj=traj, en=energy,
                                                                      tn=traj_alias), shell=True)
            except subprocess.CalledProcessError as exc:
                pot_code = get_pot_code(exc.output)
            subprocess.call('echo "{pcode} 0" | {gmx} {en} -f {fname}/{tn}-ener '
                            '-o {fname}/{tn}-energy >> rerun.log 2>&1'.format(gmx=gmx, fname=folder_name, tn=traj_alias,
                                                                              en=energy, pcode=pot_code), shell=True)
        else:
            print("in mod {fname} skipping trajectory {tn}, already calculated".format(fname=folder_name,
                                                                                       tn=traj_alias))
    else:
        if '{tn}-dhdl.xvg'.format(tn=traj_alias) not in os.listdir(folder_name):
            if tpr not in os.listdir(folder_name):
                raise RuntimeError('{}-rerun.tpr not found, aborting (check your output)'.format(top_core_name))
            print("calculating mod {fname}, trajectory {tn}".format(fname=folder_name, tn=traj_alias))
            subprocess.call('{gmx} mdrun -s {fname}/{tpr} -rerun {traj} -ntomp 1 -ntmpi 1 '
                            '-dhdl {fname}/{tn}-dhdl >> rerun.log 2>&1'.format(gmx=gmx, fname=folder_name,
                                                                               traj=traj_path, tn=traj_alias,
                                                                               tpr=tpr), shell=True)
        else:
            print("in mod {fname} skipping trajectory {tn}, already calculated".format(fname=folder_name,
                                                                                       tn=traj_alias))
        
        
def get_pot_code(err_msg):
    """
    reads output from gmx energy to locate the Potential entry
    Args:
        err_msg: str, error message from empty gmx energy run

    Returns: str, entry number corresponding to Potential

    """
    # can be problematic in Py2.7 w/decode
    return [err_msg.split()[n-1].decode("utf-8") for n in range(len(err_msg.split()))
            if err_msg.split()[n].decode("utf-8") == "Potential"][0]


def cleanup(jobs):
    """
    removes files that are no longer useful
    """
    current = os.getcwd()
    mods = [x[0] for x in jobs]
    for m in mods:
        m.goto_mydir()
        waste = ['md.log', 'traj.trr', 'ener.edr', '#']
        ldir = os.listdir('.')
        to_clean = set([x for x in ldir if any([a in x for a in waste])])
        for f in to_clean:
            os.remove(f)
    os.chdir(current)
        
        
def save_weights(orig, m):
    """
    reads energies from two reruns (original and modified),
    then calculates the FEP weights, exp(-(Em-Eo)/kT)
    and writes them to a file, once per each trajectory;
    also shifts the modified energies by the difference
    between the two (modif - orig) to avoid overflows
    in the exponential (only relative weights matter)
    :param orig: ModAtom object for the non-modified parameters
    :param m: ModAtom object for the modified parameters
    :param trajs: list of (trimmed) trajectory names
    """
    trajs = m.trajs
    orig.goto_mydir()
    energy_files = ['{}-energy.xvg'.format(tr.ordering) for tr in trajs]
    energies = {filename: read_xvg(filename, m) for filename in energy_files}
    m.goto_mydir()
    for filename, temperature, traj in [('{}-energy.xvg'.format(tr.ordering), tr.temperature, tr) for tr in trajs]:
        m_energy = read_xvg(filename, m)[:, 1]
        mean_diff_energy = np.mean(m_energy - energies[filename][:, 1])  # TODO is this OK?
        energies[filename][:, 1] *= -1
        energies[filename][:, 1] += m_energy - mean_diff_energy
        energies[filename][:, 1] = np.exp(-energies[filename][:, 1]/(0.008314*temperature))
        traj.weights = energies[filename]


def read_xvg(file, mod):
    """
    reads data from an .xvg file
    :param file: file to read
    :param mod: any Mod object
    :return: np.array object containing values in individual frames
    """
    try:
        data = mod.remove_duplicates([x.strip() for x in open(file) if not x.startswith(('#', '@', '&'))])
        return np.array([[float(line.split()[0]), float(line.split()[1])] for line in data])
    except OSError:
        print("Terminating: file {c}/{f} not found. \nCheck {c}/rerun.log for errors".format(c=os.getcwd(), f=file))
        sys.exit(1)


def try_mkdir(dirname):
    """
    attempts to create a directory, catches exception if needed
    :param dirname: dir to create
    :return:
    """
    try:
        os.mkdir(dirname)
    except OSError:
        print("the dir '{}' already exists, will overwrite/modify contents...".format(dirname))


def run_parallel(nproc, jobs, clean=True):
    """
    creates a pool of workers that perform reruns in parallel
    :param nproc: int, allowed number of processes
    :param jobs: tuple, stack of job parameters to pass to the main fn
    :param clean: bool, whether to delete residual files (to save disk space) or not (for debugging purposes)
    """
    import random
    jobs = filter_completed(jobs)
    with ProcessPoolExecutor(nproc) as executor:
        executor.map(run_gromacs, random.sample(jobs, k=len(jobs)))
    if clean:
        cleanup(jobs)


def filter_completed(jobs):
    completed = []
    for n, job in enumerate(jobs):
        structs_list, dyn_mdp, traj, gmx, alch = job
        m = traj.mod
        traj_alias = traj.ordering
        folder_name = '/'.join(m.topname.split('/')[:-1])
        os.chdir(folder_name)
        if '{tn}-dhdl.xvg'.format(tn=traj_alias) in os.listdir(folder_name):
            completed.append(n)
            print("in mod {fname} skipping trajectory {tn}: already calculated".format(fname=folder_name,
                                                                                       tn=traj_alias))
    completed.sort(reverse=True)
    for i in completed:
        jobs.pop(i)
    return jobs


def read_quantity():
    """
    reads w_binsorter output
    :return: 1D grid and the corresponding quantity values
    """
    qt = np.loadtxt('quantity.out')
    return qt[:, 0], qt[:, 1]

    
def parse_input_line(line, top, td):
    """
    reads line from input and creates the requested amount of Mod
    subclass instances corresponding to the desired configuration.
    :param line: str, line from input
    :param top: a Top object that will be passed to create the instance
    :param td: the main ThermoDiff object
    :return: a list of Mod subclass instances
    """
    line_list = line.split()
    new_mods = []
    c = line_list[1].count('-')
    mods = line_list[0]
    # case 1: dealing with charge/sigma/epsilon
    if all(x in 'cse' for x in mods):
        for mod in mods:
            ll = None
            if c == 0:
                ll = top.find_type(line_list[1])
            if c == 2:
                ll = line_list[1:]
            if c == 2 or c == 0:
                n = [entry.split('-')[2] for entry in ll]
                rn = [int(entry.split('-')[1]) for entry in ll]
                r = [entry.split('-')[0] for entry in ll]
            elif c == 1:
                n = line_list[2:]
                rn = [int(line_list[1].split('-')[1])] * len(n)
                r = [line_list[1].split('-')[0]] * len(n)
            else:
                raise ValueError("input line {} cannot be processed".format(' '.join(line_list)))
            new_mods.append(ModAtom(top=deepcopy(top), master=td, resnr=rn, names=n, resnames=r, changes=mod))
    # case 2: dealing with angles/dihedrals
    elif all(x[0] in 'ad' for x in mods):
        pars_nums = {'a': 3, 'd': 4}  # need 3 atoms per angle and 4 per dihedral
        if 'd' in mods:
            if [c for c in mods if c.isnumeric()]:
                mods = ['d'+i for i in [c for c in mods if c.isnumeric()]]
        for mod in mods:
            pars = pars_nums[mod[0]]
            if c == 0:
                top[0].get_dicts()
                print("WARNING: only using the first topology to select affected atoms")  # TODO fix this!!!
                n, rn, r = [], [], []
                ll = line_list[1:]
                assert len(ll) == pars
                sections = top[0].list_sections('dihedrals') if line.strip().startswith('d') \
                    else top[0].list_sections('angles')
                for s in sections:
                    molname = [x for x in top[0].mols.keys() if s in top[0].mols[x]][0]
                    for lnum in range(len(top[0].sections[s])):
                        lspl = top[0].sections[s][lnum].split()
                        if len(lspl) > (pars-1) and lspl[0][0] not in '[;' \
                                and check_line([top[0].nums_to_types[molname][lspl[i]] for i in range(pars)], ll):
                            n.append(tuple(top[0].nums_to_names[molname][lspl[i]].split('-')[2] for i in range(pars)))
                            rn.append(tuple(int(top[0].nums_to_names[molname][lspl[i]].split('-')[1])
                                            for i in range(pars)))
                            r.append(tuple(top[0].nums_to_names[molname][lspl[i]].split('-')[0] for i in range(pars)))
                if not n:
                    print("The requested combination of types, {}, was not found in the topology file;"
                          "the request will have no effect")
                    return []
            elif c == 1:
                ll = line_list[2:]
                check_mod(mod[0], ll)
                res = line_list[1].split('-')[0]
                num = int(line_list[1].split('-')[1])
                entries = int(len(ll) / pars)  # number of entries to process
                n = [tuple(entry for entry in ll[e*pars:(e+1)*pars])
                     for e in range(entries)]
                rn = [tuple(num for _ in ll[e * pars:(e + 1) * pars])
                      for e in range(entries)]
                r = [tuple(res for _ in ll[e * pars:(e + 1) * pars])
                     for e in range(entries)]
            elif c == 2:
                ll = line_list[1:]
                check_mod(mod[0], ll)
                entries = int(len(ll)/pars)  # number of entries to process
                n = [tuple(entry.split('-')[2] for entry in ll[e*pars:(e+1)*pars])
                     for e in range(entries)]
                rn = [tuple(int(entry.split('-')[1]) for entry in ll[e*pars:(e+1)*pars])
                      for e in range(entries)]
                r = [tuple(entry.split('-')[0] for entry in ll[e*pars:(e+1)*pars])
                     for e in range(entries)]
            else:
                raise ValueError("input line {} cannot be processed".format(' '.join(line_list)))
            new_mods.append(ModParam(top=deepcopy(top), master=td, resnr=rn, names=n, resnames=r, changes=mod))
    # case 3: dealing with nbfix sigma/epsilon
    elif all(x in 'nm' for x in mods):
        delim = ';'
        for mod in mods:
            if c == 0:
                t1 = line_list[1]
                t2 = line_list[2]
                n, rn, r = [], [], []
                for t in top:
                    ll1 = t.find_type(t1)
                    ll2 = t.find_type(t2)
                    n.append([tuple(x.split('-')[2] for x in ll1), tuple(x.split('-')[2] for x in ll2)])
                    rn.append([tuple(int(x.split('-')[1]) for x in ll1), tuple(int(x.split('-')[1]) for x in ll2)])
                    r.append([tuple(x.split('-')[0] for x in ll1), tuple(x.split('-')[0] for x in ll2)])
            elif c == 1:
                assert line.count(delim) == 1 and len(top) == 1  # TODO not clear how to implement for more tops, think
                line_first, line_second = line.split(delim)
                line_first, line_second = line_first.split(), line_second.split()
                ll1 = line_first[2:]
                ll2 = line_second[:]
                num = int(line_list[1].split('-')[1])
                res = line_list[1].split('-')[0]
                n = [[tuple(ll1), tuple(ll2)]]
                rn = [[tuple(num for _ in ll1), tuple(num for _ in ll2)]]
                r = [[tuple(res for _ in ll1), tuple(res for _ in ll2)]]
            elif c == 2:
                assert line.count(delim) == 1 and len(top) == 1
                line_first, line_second = line.split(delim)
                line_first, line_second = line_first.split(), line_second.split()
                ll1 = line_first[1:]
                ll2 = line_second[:]
                n = [[tuple(x.split('-')[2] for x in ll1), tuple(x.split('-')[2] for x in ll2)]]
                rn = [[tuple(int(x.split('-')[1]) for x in ll1), tuple(x.split('-')[1] for x in ll2)]]
                r = [[tuple(x.split('-')[0] for x in ll1), tuple(x.split('-')[0] for x in ll2)]]
            else:
                raise ValueError("input line {} cannot be processed".format(' '.join(line_list)))
            new_mods.append(ModNbfix(top=deepcopy(top), master=td, resnr=rn, names=n, resnames=r, changes=mod))
    elif any(x not in 'csenNad' for x in mods):
        raise ValueError("Input directive '{}' contains unrecognized or illegal options".format(line_list[0]))
    else:
        raise ValueError("Input directive '{}' contains illegal combination of options."
                         "Legal combinations include c/s/e and m/n/a/d".format(line_list[0]))
    return new_mods


class Target:
    pass


def process_target_line(line, abs_path, trajset_dict, td):
    """
    reads line from target.dat and returns relevant input data
    :param line: str, line to process
    :param abs_path: str, absolute path to target.dat
    :param trajset_dict: dict, binds trajectory indices to trajectory group names
    :param td: the main ThermoDiff object
    :return: a Target instance
    """
    lspl = line.strip().split()
    modes = lspl[0]
    if not all(['=' in x for x in lspl[1:]]):
        raise SyntaxError("In line {}: spaces are not allowed next to equal signs")
    ldict = {x.split('=')[0]: x.split('=')[1] for x in lspl[1:]}
    possible_keywords = ['ref_profile', 'ref_discrete', 'threshold', 'label', 'data', 'meta', 'trajset', 'relative']
    if any(x not in possible_keywords for x in ldict.keys()):
        raise ValueError('line {} contains a disallowed keyword\n'
                         'Allowed keywords are: {}'.format(line, ', '.join(possible_keywords)))
    try:
        label = ldict['label']
    except KeyError:
        label = '-'.join(ldict.values())
    try:
        trajset = trajset_dict[ldict['trajset']]
    except KeyError:
        assert len(trajset_dict.keys()) == 1
        trajset = trajset_dict[[x for x in trajset_dict.keys()][0]]
    meta = append_abs_path(ldict['meta'], abs_path)
    try:
        orig_data = append_abs_path(ldict['data'], abs_path)
    except KeyError:
        orig_data = None
    target_data, threshold = None, None
    if 'p' in modes:
        if td.calc_sens:
            target_data = append_abs_path(ldict['ref_profile'], abs_path)
        if 'threshold' in ldict.keys():
            print("The 'threshold' option is not compatible with profile set as target, will be ignored")
    elif 'd' in modes:
        if td.calc_sens:
            target_data = append_abs_path(ldict['ref_discrete'], abs_path)
        try:
            threshold = append_abs_path(ldict['threshold'], abs_path)
        except KeyError:
            threshold = None
    else:
        raise ValueError('line {} does not specify the type of reference data '
                         '(profile, average or difference)'.format(line))
    t = Target()
    # now everything below is a string or None, or a Trajset instance
    t.mode, t.label, t.target_data, t.orig_data, t.threshold, t.meta, t.trajset = modes, label, target_data, \
        orig_data, threshold, meta, trajset
    return t


def process_targets(input_file, trajs_input, td):
    """
    Top-level fn for input processing, parses options for
    target refinement
    :param input_file: str, input file for targets
    :param trajs_input: str, input file for trajectories
    :param td: the main ThermoDiff object
    :return: list, contains tuples of directives
    """
    trajs, trajsets = read_traj_params(trajs_input)
    targets = []
    input_path = '/'.join(input_file.split('/')[:-1])
    for line in open(input_file):
        targets.append(process_target_line(line, input_path, trajsets, td))
    return targets, trajs


def read_traj_params(file):
    """
    Reads the whole Trajectory input file and processes it to yield
    a list of Traj objects and a dictionary that binds trajset names
    to Trajset objects for further use by process_targets
    :param file: str, path to the trajectory input file
    :return: list, list of Traj instances
             dict, contains bindings to Trajset instances
    """
    trajsets = {'all': Trajset('all')}
    curr_name = 'all'
    content = [line.strip() for line in open(file) if line.strip()]
    base_path = '/' + '/'.join(file.split('/')[:-1]) + '/'
    trajs = []
    nline = 0
    for line in content:
        if line.startswith(':'):
            curr_name = line.strip(':').strip()
            trajsets[curr_name] = Trajset(curr_name)
        else:
            lspl = line.split()
            if not lspl[0].startswith('/'):
                lspl[0] = base_path + lspl[0]
            if len(lspl) == 3:
                trajs.append(
                    Traj(ordering='t' + str(nline), path=lspl[0], temperature=float(lspl[1]), top_num=int(lspl[2])))
            elif len(lspl) == 1:
                trajs.append(Traj(ordering='t' + str(nline), path=lspl[0]))
            else:
                raise RuntimeError("Expected 1 or 3 parameters on an input line\n{}\n, got {}".format(len(line), line))
            trajsets[curr_name].append(trajs[-1])
            nline += 1
    return trajs, {k: trajsets[k] for k in trajsets.keys() if trajsets[k]}

    
def check_mod(mod, list_atoms):
    """
    checks whether the number of input lines matches the desired modification
    :param mod: one-letter code for modifications
    :param list_atoms: list of atoms to check against
    :return: None
    """
    ok = False
    if mod == 'a':
        ok = len(list_atoms) % 3 == 0
    elif mod == 'd':
        ok = len(list_atoms) % 4 == 0
    elif mod == 'n' or mod == 'N':
        ok = len(list_atoms) % 2 == 0
    if not ok:
        raise ValueError("mod {} contains an inappropriate number "
                         "of atom/type specifiers: {}".format(mod, len(list_atoms)))
    

def append_abs_path(filename, path):
    if filename.startswith('/'):
        return filename
    else:
        return path + '/' + filename
    

def check_line(list_line_to_check, list_entries):
    """
    checks if two lines share the same parameter entries
    (possibly in reverse order)
    :param list_line_to_check: list, line from topology file split at whitespaces
    :param list_entries: list, contains desired atomtypes as strings
    :return: bool, True if match found
    """
    n = len(list_entries)
    ok = all([list_line_to_check[i] == list_entries[i] for i in range(n)]) \
        or all([list_line_to_check[i] == list_entries[n - i - 1] for i in range(n)])
    return ok


def plot_results(mods, q_num, axis):
    for m in range(len(mods)):
        axis.bar(x=m, height=mods[m].qs[q_num].target_improvement_per_unit_change, label=str(m))
    axis.legend()
    #  TODO draw profile
    #  TODO draw histogram


class OptParserEmul:
    """
    written for consistency with the wham optparser to emulate its behavior;
    default values should be ok in most cases
    """
    # TODO make temperatures consistent with trajs input
    def __init__(self, nbins=50, temperature=300, tolerance=0.00001):
        self.n = nbins
        self.t = temperature
        self.tol = tolerance
