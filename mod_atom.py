from mod import Mod


class ModAtom(Mod):
    def __init__(self, top, master, resnr='', resnames='', names='', changes=''):
        """
        Subclass to work with modified atomic params (that is,
        sigma and epsilon defined for individual types, or charge defined
        for individual atoms).
        The class takes in a set of atoms that all correspond
        to a single atom type (has to be the same for all atoms);
        these atoms' type will be cloned from "type" to "Ytype",
        and the corresponding sigma/epsilon/charge parameters will be modified.
        :param top: a Top object corresponding to the modified topology
        :param resnr: a list of residue numbers
        :param resnames: a list of residue names
        :param names: a list of of atom names
        :param changes: single character denoting the change to be performed
        """
        super(ModAtom, self).__init__(top, master, resnr, resnames, names)
        self.sigma = 's' if 's' in changes else ''
        self.eps = 'e' if 'e' in changes else ''
        self.chg = 'c' if 'c' in changes else ''
        # TODO fix charge formatting
        if not (self.chg or self.eps or self.sigma):
            raise ValueError("no mode selected appropriate for an ModAtom object")
        if names and resnr:
            for t in self.top:
                self.types_are_consistent(t)
                self.type = self.get_type(t, self.names[0], self.res[0], self.resnames[0])
                if not self.chg:
                    self.clone_type(t, atomtype=self.type, prefix='Y')
                self.sort_dihedrals(t)
                self.mod_sigma_eps(t)
                self.mod_chg(t)
        else:
            self.names = ['orig']
            self.res = 0
    
    def mod_chg(self, top):
        """
        looks for a line in topology section 'atoms'
        and modifies it as needed
        :return: None
        """
        for x in range(len(self.names)):
            name = self.names[x]
            resname = self.resnames[x]
            resnum = self.res[x]
            for section in top.list_sections('atoms'):
                for l in range(len(top.sections[section])):
                    lspl = top.sections[section][l].split()
                    if len(lspl) >= 7 and lspl[4] == name and int(lspl[2]) == resnum and lspl[3] == resname:
                        modline = self.increment_charge_line(lspl)
                        top.sections[section][l] = modline
    
    def increment_charge_line(self, cont_list):
        """
        takes a line from the topology section 'atoms'
        and increases charge by self.dpar;
        also changes atomtype to modified (Y-prefixed)
        :param cont_list: line passed as a list of non-whitespace strings
        :return: an assembled line with type and/or charge modified
        """
        cont_list[6] = float(cont_list[6])
        if len(cont_list) == 7:
            cont_list = cont_list + [' ']
        else:
            cont_list = cont_list[:8]
        if self.chg:
            if self.alch:
                cont_list.extend([cont_list[1], float(cont_list[6]) + self.dpar, cont_list[7]])
            else:
                cont_list[6] = float(cont_list[6]) + self.dpar
        if self.sigma or self.eps:
            if self.alch:
                cont_list = cont_list[:8]
                cont_list.append('Y' + cont_list[1])
                cont_list.append(cont_list[6])
                cont_list.append(cont_list[7])
            else:
                cont_list[1] = 'Y' + cont_list[1]
        if self.alch:
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}{:>7s}{:>11.4f}{:>11s}\n'
            return fstring.format(*cont_list[:11])
        else:
            fstring = '{:>6s}{:>11s}{:>7s}{:>7s}{:>7s}{:>7s}{:>11.4f}{:>11s}\n'
            return fstring.format(*cont_list[:8])
    
    def mod_sigma_eps(self, top):
        """
        looks for a line in topology section 'atomtypes'
        and modifies it as needed
        :return: None
        """
        section = top.list_sections('atomtypes')[0]
        for l in range(len(top.sections[section])):
            lspl = top.sections[section][l].split()
            if len(lspl) >= 6 and lspl[0] == 'Y' + self.type:
                modline = self.increment_sigma_eps_line(lspl)
                top.sections[section][l] = modline
    
    def increment_sigma_eps_line(self, cont_list):
        """
        takes a line from the topology section 'atomtypes'
        and increases sigma by self.dpar
        :param cont_list: line passed as a list of non-whitespace strings
        :return: an assembled line with sigma modified
        """
        pos_A = cont_list.index("A")
        if self.sigma:
            if pos_A == 4:
                cont_list[5] = float(cont_list[5]) + self.dpar
            elif pos_A == 3:
                cont_list[4] = float(cont_list[4]) + self.dpar
            else:
                raise RuntimeError("Improper line formating: {}".format(" ".join(cont_list)))
        if self.eps:
            if pos_A == 4:
                cont_list[6] = float(cont_list[6]) + self.dpar
            elif pos_A == 3:
                cont_list[5] = float(cont_list[5]) + self.dpar
            else:
                raise RuntimeError("Improper line formating: {}".format(" ".join(cont_list)))
        if pos_A == 4:
            fstring = '{:<11s}{:>3s}{:>11s}{:>10s}{:>3s}{:>20}{:>20}'
        elif pos_A == 3:
            fstring = '{:<11s}{:>11s}{:>10s}{:>3s}{:>20}{:>20}'
        return fstring.format(*cont_list[:7])
    
    def types_are_consistent(self, top):
        """
        if many atoms are passed in a single input line, need to make sure
        they are all of the same type
        :return: True if all are of the same type, False otherwise
        """
        consistent = all([self.get_type(top, self.names[0], self.res[0], self.resnames[0])
                          == self.get_type(top, self.names[n], self.res[n], self.resnames[n])
                         for n in range(len(self.names))])
        if not consistent:
            raise ValueError("atoms within a single charge, sigma or epsilon modification need to have consistent types")
